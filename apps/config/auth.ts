/**
 * Config source: https://git.io/JY0mp
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

import { AuthConfig } from "@ioc:Adonis/Addons/Auth";

/*
|--------------------------------------------------------------------------
| Authentication Mapping
|--------------------------------------------------------------------------
|
| List of available authentication mapping. You must first define them
| inside the `contracts/auth.ts` file before mentioning them here.
|
*/
const authConfig: AuthConfig = {
  guard: "api",
  guards: {
    api: {
      driver: "oat",
      tokenProvider: {
        type: "api",
        driver: "database",
        table: "customer_tokens",
        foreignKey: "customer_id",
      },
      provider: {
        hashDriver: "argon",
        driver: "lucid",
        identifierKey: "id",
        uids: ["email"],
        model: () => import("App/Models/Customer"),
      },
    },
    api_employee: {
      driver: "oat",
      tokenProvider: {
        type: "api",
        driver: "database",
        table: "employee_tokens",
        foreignKey: "employee_id",
      },
      provider: {
        hashDriver: "argon",
        driver: "lucid",
        identifierKey: "id",
        uids: ["username"],
        model: () => import("App/Models/Employee"),
      },
    },
  },
};

export default authConfig;
