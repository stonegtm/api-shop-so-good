/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";
import CategoryMainsController from "App/Controllers/Http/CategoryMainsController";
import CategoryListsController from "App/Controllers/Http/CategoryListsController";
import ProductsController from "App/Controllers/Http/ProductsController";
import ProductDetailsController from "App/Controllers/Http/ProductDetailsController";
import StocksController from "App/Controllers/Http/StocksController";
import BrandsController from "App/Controllers/Http/BrandsController";
import CustomersController from "App/Controllers/Http/CustomersController";
import CarouselsController from "App/Controllers/Http/homepage/CarouselsController";
import NewarrivalsController from "App/Controllers/Http/homepage/NewarrivalsController";
import FeaturedBrandsController from "App/Controllers/Http/homepage/FeaturedBrandsController";
import BestSellersController from "App/Controllers/Http/homepage/BestSellersController";
import EmployeesController from "App/Controllers/Http/auth/EmployeesController";
import CartsController from "App/Controllers/Http/CartsController";
import PaymentsController from "App/Controllers/Http/PaymentsController";
import CategoryAndBrandsController from "App/Controllers/Http/CategoryAndBrandsController";
import ReviewContentsController from "App/Controllers/Http/ReviewContentsController";
import YouMayAlsoLikesController from "App/Controllers/Http/YouMayAlsoLikesController";
import SeoulgoodsPicksController from "App/Controllers/Http/SeoulgoodsPicksController";
import PromotionsController from "App/Controllers/Http/PromotionsController";



import Stock from "App/Models/Stock";


//////////////////// Only Front End ///////////////
Route.group(() => {
  Route.post("/v1/product-other-sigle", async (ctx) => {
    return new ProductsController().getProductOtherSingle(ctx);
  });
  Route.get("/v1/backend-picked-best-sellers", async (ctx) => {
    return new BestSellersController().bestSellersPickedFront(ctx);
  });
  Route.get("/v1/backend-picked-best-sellers-all", async (ctx) => {
    return new BestSellersController().bestSellersAllPickedFront(ctx);
  });
  Route.get("/v1/front-you-may-also-like", async (ctx) => {
    return new YouMayAlsoLikesController().youMayAlsoLikePickedFront(ctx);
  });
  Route.get("/v1/front-seoulgoodspick", async (ctx) => {
    return new SeoulgoodsPicksController().seoulgoodspickPickedFront(ctx);
  });
  Route.get("/v1/get-review", async (ctx) => {
    return new ReviewContentsController().getAllreview(ctx);
  });
  Route.post("/v1/get-review-single", async (ctx) => {
    return new ReviewContentsController().getSingleReview(ctx);
  });
  Route.get("/v1/category-by-codename/:code_name", async (ctx) => {
    return new CategoryAndBrandsController().getByCategoryCodeName(ctx);
  });
  Route.get("/v1/brand-by-codename/:code_name", async (ctx) => {
    return new CategoryAndBrandsController().getByBrandCodeName(ctx);
  });
  Route.get("/v1/front-show-data-promotion", async (ctx) => {
    return new PromotionsController().showPromotionFront(ctx);
  });
  Route.get("/v1/front-show-data-promotion-single/:code_name", async (ctx) => {
    return new PromotionsController().showPromotionSingleFront(ctx);
  });
  Route.post("/v1/front-show-product-promotion", async (ctx) => {
    return new PromotionsController().showPromotionProduct(ctx);
  });
  Route.get("/v1/show-review-customer", async (ctx) => {
    return new ReviewContentsController().showAllReviewCustomer(ctx);
  });
  Route.post("/v1/get-review-customer-single", async (ctx) => {
    return new ReviewContentsController().showSinglelReviewCustomer(ctx);
  });
}).prefix("/api");

//MIDDLE WARE
Route.group(() => {
  Route.get("/v1/address-for-press", async (ctx) => {
    return new PaymentsController().addressForPlace(ctx);
  });
  Route.post("/v1/favorite-product", async (ctx) => {
    return new CustomersController().favoriteProduct(ctx);
  });
  Route.post("/v1/favorite-product-picked", async (ctx) => {
    return new CustomersController().favoriteProductPicked(ctx);
  });
  Route.post("/v1/pick-product-review", async (ctx) => {
    return new ReviewContentsController().pickProductForReview(ctx);
  });
  Route.post("/v1/get-name-user", async (ctx) => {
    return new ReviewContentsController().getNameUser(ctx);
  });
  Route.post("/v1/create-review-customer", async (ctx) => {
    return new ReviewContentsController().createReviewCustomer(ctx);
  });
}).prefix("/api").middleware("auth:api");
//Image ProductDescription
Route.get("/api/v1/product_description_image/:name", async (ctx) => {
  return new ProductsController().selectImageProductDescription(ctx);
});
Route.get("/api/v1/review-image/:name", async (ctx) => {
  return new ReviewContentsController().imageReviewContent(ctx);
});
Route.get("/api/v1/promotion-image/:name", async (ctx) => {
  return new PromotionsController().showImagePromotion(ctx);
});
Route.get("/api/v1/promotion-banner-pc/:name", async (ctx) => {
  return new PromotionsController().showImageBannerPcPromotion(ctx);
});
Route.get("/api/v1/promotion-banner-mobile/:name", async (ctx) => {
  return new PromotionsController().showImageBannerMobilePromotion(ctx);
});



// Route.get("/api/v1/product-seo/:name", async (ctx) => {
//   return new ProductsController().getProductSeo(ctx);
// });
//////////////////// Only Front End ///////////////

//////////////////// Only Back End ///////////////

Route.group(() => {
  Route.post("/v1/get-product-and-address", async (ctx) => {
    return new PaymentsController().getProductAndAdress(ctx);
  });
  Route.post("/v1/product-other-sigle-backend", async (ctx) => {
    return new ProductsController().getProductOtherSingleBackend(ctx);
  });
  Route.post("/v1/edit-product-other-sigle-backend", async (ctx) => {
    return new ProductsController().editProductOtherSingleBackend(ctx);
  });
  Route.post("/v1/delete-image-product-other", async (ctx) => {
    return new ProductsController().DeletePicturePdo(ctx);
  });
  Route.post("/create-product", async (ctx) => {
    return new ProductsController().index(ctx);
  });
  Route.post("/v1/create-review", async (ctx) => {
    return new ReviewContentsController().index(ctx);
  });
  Route.post("/v1/edit-review", async (ctx) => {
    return new ReviewContentsController().editReview(ctx);
  });
  Route.get("/v1/you-may-also-like", async (ctx) => {
    return new YouMayAlsoLikesController().index(ctx);
  });
  Route.get("/v1/get-review-backend", async (ctx) => {
    return new ReviewContentsController().getReview(ctx);
  });
  Route.get("/v1/get-sigle-review-backend/:id", async (ctx) => {
    return new ReviewContentsController().getSigleReview(ctx);
  });
  Route.post("/v1/delete-review-backend", async (ctx) => {
    return new ReviewContentsController().deleteReview(ctx);
  });
  Route.post("/v1/add-you-may-also-like", async (ctx) => {
    return new YouMayAlsoLikesController().addYouMayAlsoLike(ctx);
  });
  Route.post("/v1/delete-you-may-also-like", async (ctx) => {
    return new YouMayAlsoLikesController().deleteYouMayAlsoLike(ctx);
  });
  Route.get("/v1/back-seoulgoodspick", async (ctx) => {
    return new SeoulgoodsPicksController().index(ctx);
  });
  Route.post("/v1/add-seoulgoodspick", async (ctx) => {
    return new SeoulgoodsPicksController().addSeoulgoodPick(ctx);
  });
  Route.post("/v1/delete-seoulgoodspick", async (ctx) => {
    return new SeoulgoodsPicksController().deleteSeoulgoodspick(ctx);
  });
  Route.post("/v1/add-promotion", async (ctx) => {
    return new PromotionsController().addPromotion(ctx);
  });
  Route.get("/v1/back-show-data-promotion", async (ctx) => {
    return new PromotionsController().showBackend(ctx);
  });
  Route.get("/v1/back-show-data-promotion-single/:code_name", async (ctx) => {
    return new PromotionsController().showPromotionSingleBackend(ctx);
  });
  Route.post("/v1/back-add-product-promotion", async (ctx) => {
    return new PromotionsController().addProductPromotion(ctx);
  });
  Route.post("/v1/get-promotion-product-picked", async (ctx) => {
    return new PromotionsController().getProductPromotionPicked(ctx);
  });
  Route.post("/v1/edit-discount-product-promotion", async (ctx) => {
    return new PromotionsController().editDiscountProductPromotion(ctx);
  });
  Route.post("/v1/delete-product-promotion", async (ctx) => {
    return new PromotionsController().deleteProductPromotion(ctx);
  });
  Route.get("/v1/name-for-add-product-promotion", async (ctx) => {
    return new PromotionsController().nameProductForAdd(ctx);
  });
  Route.post("/v1/promotion_other_product", async (ctx) => {
    return new PromotionsController().productOtherPromotion(ctx);
  });
  Route.post("/v1/add_promotion_other_product", async (ctx) => {
    return new PromotionsController().promotionProductOther(ctx);
  });
  Route.post("/v1/get_promotion_single", async (ctx) => {
    return new PromotionsController().showPromotionProductSingle(ctx);
  });
}).prefix("/api")
  .middleware("auth:api_employee");

//////////////////// Only Back End ///////////////


Route.group(() => {
  //Province
  Route.get("/v1/province-for-address", async (ctx) => {
    return new PaymentsController().provinceSelect(ctx);
  });
  //CREATE CATEGORIRES START
  Route.post("/create-categories", async (ctx) => {
    return new CategoryMainsController().index(ctx);
  });
  Route.post("/create-categories-second", async (ctx) => {
    return new CategoryListsController().index(ctx);
  });

  Route.post("/v1/add-product-other", async (ctx) => {
    return new ProductsController().addProductOhter(ctx);
  });
  Route.get("/v1/get-product-other/:id", async (ctx) => {
    return new ProductsController().getOther(ctx);
  });
  Route.post("/create-product-detail", async (ctx) => {
    return new ProductDetailsController().index(ctx);
  });
  Route.post("/create-stock", async (ctx) => {
    return new StocksController().index(ctx);
  });
  Route.post("/create-brand", async (ctx) => {
    return new BrandsController().index(ctx);
  });

  //CREATE CATEGORIRES END

  //Search Product START
  Route.post("/v1/search-product", async (ctx) => {
    return new ProductsController().searchProduct(ctx);
  });
  Route.post("/v1/search-product-page", async (ctx) => {
    return new ProductsController().searchProductPage(ctx);
  });
  //END Product End

  //SELECT CATEGORIRES START
  //////////////////////////////
  //////////JOIN TABLE//////////
  //////////////////////////////
  Route.get("/categories-main-with-list", async (ctx) => {
    return new CategoryListsController().selectJoinCategories(ctx);
  });
  //////////////////////////////
  //////////JOIN TABLE//////////
  //////////////////////////////

  ///SELECT CATEGORY LIST BY ID FOR ADD PRODUCT
  Route.post("/categories-list-for-product", async (ctx) => {
    return new CategoryListsController().selectCategoryByName(ctx);
  });
  ///SELECT CATEGORY LIST BY NAME
  Route.post("/categories-name-by-id", async (ctx) => {
    return new CategoryMainsController().selectCategoryByName(ctx);
  });

  ////////////////////////////////////////////////////////////ONLY **NANE** CATEGORY MAIN START
  Route.get("/categories", async (ctx) => {
    return new CategoryMainsController().categoriesForSelect(ctx);
  });
  ////////////////////////////////////////////////////////////ONLY **NANE** CATEGORY MAIN END
  ///////////////////////////////////////////////////////////SELECT ALL CATEGORIES***
  Route.get("/categories-main", async (ctx) => {
    return new CategoryMainsController().categoriesMain(ctx);
  });
  //////
  Route.get("/categories-for-product", async (ctx) => {
    return new CategoryMainsController().categoriesForProduct(ctx);
  });
  ///////////////////////////////////////////////////////////SELECT ALL CATEGORIES***
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES MAIN***
  Route.post("/categories-main-by-id", async (ctx) => {
    return new CategoryMainsController().selectByid(ctx);
  });
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES MAIN***
  ///////////////////////////////////////////////////////////SELECT ALL CATEGORIES***
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES MAIN***
  Route.post("/categories-list-by-id", async (ctx) => {
    return new CategoryListsController().selectByid(ctx);
  });
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES MAIN***
  //SELECT IMG CATEGORY MAIN START สามารถเรียกรูปไปใช้ผ่าน api
  Route.get("/categories-main-image/:name", async (ctx) => {
    return new CategoryMainsController().selectImageMain(ctx);
  });
  //SELECT IMG CATEGORY MAIN END  สามารถเรียกรูปไปใช้ผ่าน api

  //CATEGORIES LIST//***** */

  //
  //
  //SELECT CATEGORY LIST FOR NANE START
  Route.post("/categories-list", async (ctx) => {
    return new CategoryListsController().selectCategories(ctx);
  });
  //SELECT CATEGORY LIST FOR NANE START

  //SELECT CATEGORIRES END
  //
  //
  ////////////////////////////////////////////////////////////// EDIT
  //EDIT CATEGORIES MAIN START
  Route.post("/categories-main-edit", async (ctx) => {
    return new CategoryMainsController().editCategories(ctx);
  });
  Route.post("/category-edit-status-only", async (ctx) => {
    return new CategoryMainsController().editStatusOnly(ctx);
  });
  //EDIT CATEGORIES MAIN END
  //EDIT CATEGORIES LIST START
  Route.post("/categories-list-edit", async (ctx) => {
    return new CategoryListsController().editCategoriesList(ctx);
  });
  //EDIT CATEGORIES LIST END
  ////////////////////////////////////////////////////////////// EDIT

  //
  //
  //DELETE CATEGORIES MAIN START
  Route.post("/categories-main-delete", async (ctx) => {
    return new CategoryMainsController().deleteCategories(ctx);
  });
  //DELETE CATEGORIES MAIN END
  //DELETE CATEGORIES LIST START
  Route.post("/categories-list-delete", async (ctx) => {
    return new CategoryListsController().deleteCategoriesList(ctx);
  });
  //DELETE CATEGORIES LIST END
}).prefix("/api");

//GROUPIMG
Route.group(() => {
  //SELECT IMG CATEGORY LIST START สามารถเรียกรูปไปใช้ผ่าน api
  Route.get("/categories-list-image/:name", async (ctx) => {
    return new CategoryListsController().selectImageList(ctx);
  });
  //SELECT IMG CATEGORY LIST END  สามารถเรียกรูปไปใช้ผ่าน api
}).prefix("/api");

//BRANDS
//GROUP EDIT
Route.group(() => {
  //SELECT ALL
  Route.get("/all", async (ctx) => {
    return new BrandsController().selectAll(ctx);
  });
  Route.post("/edit-status", async (ctx) => {
    return new BrandsController().editBrands(ctx);
  });
  //DELETE BRANDS
  Route.post("/delete", async (ctx) => {
    return new BrandsController().deleteBrands(ctx);
  });
}).prefix("/api/v1/brands");

//GROUP IMAGE
Route.group(() => {
  Route.get("/picture/brands-img/:name", async (ctx) => {
    return new BrandsController().selectImageBrands(ctx);
  });
  Route.get("/brands", async (ctx) => {
    return new BrandsController().selectBrands(ctx);
  });
  Route.post("/brands/by-id", async (ctx) => {
    return new BrandsController().selectOneById(ctx);
  });
}).prefix("/brands/v1");

//Feature-Brand Start
Route.get("/api/v1/feature-img/:name", async (ctx) => {
  return new FeaturedBrandsController().imageFeatureForShow(ctx);
});
Route.get("/api/v1/feature-brand7789c5", async (ctx) => {
  return new FeaturedBrandsController().getFeature(ctx);
});
Route.get("/api/v1/feature-front-brand7789c5", async (ctx) => {
  return new FeaturedBrandsController().getFeatureFront(ctx);
});
//Feature-Brand End
//Slip Image Start
Route.get("/api/v1/slip-img/:name", async (ctx) => {
  return new PaymentsController().imageSlipShow(ctx);
});
Route.get("/api/v1/product-other/:name", async (ctx) => {
  return new ProductsController().showProductOther(ctx);
});
//Slip Image End

//API PRODUCT

Route.group(() => {
  Route.get("/get-all", async (ctx) => {
    return new ProductsController().getProductMain(ctx);
  });
  Route.get("/picture-product/:name", async (ctx) => {
    return new ProductsController().getImageProduct(ctx);
  });
  Route.get("/new-arrival", async (ctx) => {
    return new ProductsController().getProductNewArrival(ctx);
  });
  Route.post("/product-single-test-backend", async (ctx) => {
    return new ProductsController().getProductTest(ctx);
  });
  Route.post("/product-color", async (ctx) => {
    return new ProductsController().getProductColor(ctx);
  });
  Route.post("/product-single", async (ctx) => {
    return new ProductsController().getProductShowSingle(ctx);
  });
  Route.post("/get-product-other", async (ctx) => {
    return new ProductsController().getProductOtherData(ctx);
  });
  Route.get("/get-product/:id", async (ctx) => {
    return new ProductsController().getProduct(ctx);
  });
  Route.post("/edit-quantility", async (ctx) => {
    return new StocksController().editQuantility(ctx);
  });
  Route.post("/show-image", async (ctx) => {
    return new ProductsController().getNameImageByProductId(ctx);
  });
  Route.post("/delete-image", async (ctx) => {
    return new ProductsController().deleteImage(ctx);
  });
  Route.post("/delete-image-detail", async (ctx) => {
    return new ProductsController().deleteImageDetail(ctx);
  });
  Route.post("/add-image-other", async (ctx) => {
    return new ProductsController().addImageProductOther(ctx);
  });
  Route.post("/add-image-detail-other", async (ctx) => {
    return new ProductsController().addImageDetailProductOther(ctx);
  });
  Route.post("/edit-main-image", async (ctx) => {
    return new ProductsController().editMainImage(ctx);
  });
  Route.post("/edit-data-product", async (ctx) => {
    return new ProductsController().editDataProduct(ctx);
  });
  Route.post("/edit-status-product", async (ctx) => {
    return new ProductsController().editStatusProduct(ctx);
  });
  Route.post("/delete-product", async (ctx) => {
    return new ProductsController().deleteProduct(ctx);
  });
}).prefix("api/v1/product");


Route.group(() => {

}).prefix("/api")
  .middleware("auth:api");
//Image ForEdit ดึงภาพออกไปโชว์
Route.group(() => {
  Route.get("/picture-product/:name", async (ctx) => {
    return new ProductsController().getImageProduct(ctx);
  });
}).prefix("product/edit/v1");

//Custommer//Custommer//Custommer
Route.post("api/v1/check-register", async (ctx) => {
  return new CustomersController().checkRegister(ctx);
});
Route.post("api/v1/login-customer", async (ctx) => {
  return new CustomersController().loginCustomer(ctx);
});
Route.post("api/v1/login-customer-facebook", async (ctx) => {
  return new CustomersController().loginFacebook(ctx);
});
Route.post("api/v1/register", async (ctx) => {
  return new CustomersController().createCustomer(ctx);
});
Route.group(() => {
  Route.get("/customer-data", async (ctx) => {
    return new CustomersController().getUsername(ctx);
  });
  Route.post("/logout-customer", async (ctx) => {
    return new CustomersController().logoutCustomer(ctx);
  });
  Route.post("/update-profile", async (ctx) => {
    return new CustomersController().updateProfile(ctx);
  });
  Route.get("/get-profile", async (ctx) => {
    return new CustomersController().getProfile(ctx);
  });
  Route.post("/add-new-address", async (ctx) => {
    return new CustomersController().addNewAdress(ctx);
  });
  Route.post("/update-address", async (ctx) => {
    return new CustomersController().updateAdress(ctx);
  });
  Route.post("/get-by-address", async (ctx) => {
    return new CustomersController().getAdressById(ctx);
  });
  Route.get("/get-cart", async (ctx) => {
    return new CustomersController().getCart(ctx);
  });
  Route.get("/get-address", async (ctx) => {
    return new CustomersController().getAdress(ctx);
  });
  Route.post("/checkout", async (ctx) => {
    return new PaymentsController().placeOrder(ctx);
  });
  Route.get("/get-payment", async (ctx) => {
    return new PaymentsController().paymentOrderSelect(ctx);
  });
  Route.post("/get-slip-pic", async (ctx) => {
    return new PaymentsController().getPictureSlip(ctx);
  });
  Route.post("/change-slip-pic", async (ctx) => {
    return new PaymentsController().changePictureSlip(ctx);
  });
})
  .prefix("api/v1")
  .middleware("auth:api");

//Custommer//Custommer//Custommer
Route.post("api/v1/employee-create", async (ctx) => {
  return new EmployeesController().createEmployee(ctx);
});
Route.group(() => {
  Route.post("/product-other-image", async (ctx) => {
    return new ProductsController().imageProductOther(ctx);
  });
  Route.post("/add-feature-brand", async (ctx) => {
    return new FeaturedBrandsController().createFeatureBrand(ctx);
  });
  Route.post("/edit-and-delete-feature-brand", async (ctx) => {
    return new FeaturedBrandsController().editAndDeleteFeatureBrand(ctx);
  });
  Route.post("/customer-profile", async (ctx) => {
    return new CustomersController().getProfileSingle(ctx);
  });
  Route.get("/payment-backend", async (ctx) => {
    return new PaymentsController().getPaymentBackend(ctx);
  });
  Route.post("/add-note-product", async (ctx) => {
    return new PaymentsController().addNoteProduct(ctx);
  });
  Route.post("/cut-product-in-stock", async (ctx) => {
    return new PaymentsController().cutProductInStock(ctx);
  });
  Route.post("/edit-status-payment", async (ctx) => {
    return new PaymentsController().editStatusPayment(ctx);
  });
  Route.get("/employe-data", async (ctx) => {
    return new EmployeesController().getEmployee(ctx);
  });
  Route.post("/logout-employee", async (ctx) => {
    return new EmployeesController().logoutEmployee(ctx);
  });
  Route.get("/customer-backend", async (ctx) => {
    return new CustomersController().customerData(ctx);
  });
  Route.get("/customer-address/:id", async (ctx) => {
    return new CustomersController().addressCustomer(ctx);
  });
  Route.get("/customer-incart/:id", async (ctx) => {
    return new CartsController().inCartBackend(ctx);
  });
})
  .prefix("api/v1")
  .middleware("auth:api_employee");

Route.post("api/v1/login-employee", async (ctx) => {
  return new EmployeesController().loginEmployee(ctx);
});
//HOME API
Route.get("homepage/v1/image-carousel/:name", async (ctx) => {
  return new CarouselsController().selectCarouselImage(ctx);
});
Route.get("homepage/v1/carousel-image", async (ctx) => {
  return new CarouselsController().selectCarouselNameImage(ctx);
});
Route.group(() => {
  Route.post("/v1/carousel-add", async (ctx) => {
    return new CarouselsController().index(ctx);
  });

  Route.post("/v1/carousel-edit", async (ctx) => {
    return new CarouselsController().editDataCarousel(ctx);
  });
  Route.post("/v1/carousel-delete", async (ctx) => {
    return new CarouselsController().deleteCarousel(ctx);
  });
  Route.post("/v1/edit-newarrival", async (ctx) => {
    return new NewarrivalsController().editNewArrival(ctx);
  });
  Route.post("/v1/edit-rating", async (ctx) => {
    return new NewarrivalsController().editRatingNewArrival(ctx);
  });
  Route.get("/v1/backend-carousel-image", async (ctx) => {
    return new CarouselsController().selectCarouselBackend(ctx);
  });
  Route.get("/v1/backend-best-sellers", async (ctx) => {
    return new BestSellersController().bestSellersGet(ctx);
  });
  Route.get("/v1/backend-picked-best-sellers", async (ctx) => {
    return new BestSellersController().bestSellersPicked(ctx);
  });
  Route.get("/v1/name-product-for-add", async (ctx) => {
    return new BestSellersController().nameProductForAdd(ctx);
  });
  Route.post("/v1/backend-add-best-sellers", async (ctx) => {
    return new BestSellersController().addBestSellers(ctx);
  });
  Route.post("/v1/backend-delete-best-sellers", async (ctx) => {
    return new BestSellersController().deletePickedBestSellers(ctx);
  });

})
  .prefix("homepage")
  .middleware("auth:api_employee");

Route.group(() => {
  Route.post("/add-item-to-cart", async (ctx) => {
    return new CartsController().addItemToCart(ctx);
  });
  Route.post("/quantity-cart", async (ctx) => {
    return new CustomersController().updownQuantity(ctx);
  });
  Route.post("/delete-order", async (ctx) => {
    return new CustomersController().deleteCart(ctx);
  });
})
  .prefix("api/cart/v1")
  .middleware("auth:api");
