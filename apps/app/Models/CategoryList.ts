import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class CategoryList extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "name" })
  public name: string;
  @column({ columnName: "displayed" })
  public displayed: boolean;
  @column({ columnName: "main" })
  public main: boolean;
  @column({ columnName: "id_category_main" })
  public id_category_main: number;
  @column({ columnName: "img_cover" })
  public img_cover: string;
  @column({ columnName: "meta_title" })
  public meta_title: string;
  @column({ columnName: "meta_description" })
  public meta_description: string;
  @column({ columnName: "meta_keywords" })
  public meta_keywords: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
