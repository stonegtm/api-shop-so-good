import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class CustomerToken extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "username" })
  public username: string;
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
