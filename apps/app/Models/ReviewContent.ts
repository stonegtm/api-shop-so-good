import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ReviewContent extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "title" })
  public title: string;
  @column({ columnName: "title_code" })
  public title_code: string;
  @column({ columnName: "description_short" })
  public description_short: string;
  @column({ columnName: "description_full" })
  public description_full: string;
  @column({ columnName: "image" })
  public image: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
