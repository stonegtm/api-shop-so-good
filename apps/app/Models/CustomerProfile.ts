import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CustomerProfile extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "fullname" })
  public fullname: string;
  @column({ columnName: "phone_number" })
  public phone_number: string;
  @column({ columnName: "day" })
  public day: string;
  @column({ columnName: "month" })
  public month: string;
  @column({ columnName: "year" })
  public year: string;
  @column({ columnName: "sex" })
  public sex: number;
  
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
