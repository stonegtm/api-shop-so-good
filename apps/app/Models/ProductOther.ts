import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductOther extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "price" })
  public price: number;
  @column({ columnName: "discount" })
  public discount: number;
  @column({ columnName: "color" })
  public color: string;
  @column({ columnName: "code_color" })
  public code_color: string;
  @column({ columnName: "size" })
  public size: string;
  @column({ columnName: "code_size" })
  public code_size: string;
  @column({ columnName: "quantity" })
  public quantity: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
