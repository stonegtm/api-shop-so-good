import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CustomerAddress extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "address" })
  public address: string;
  @column({ columnName: "parish" })
  public parish: string;
  @column({ columnName: "district" })
  public district: string;
  @column({ columnName: "province" })
  public province: string;
  @column({ columnName: "zipcode" })
  public zipcode: string;
  @column({ columnName: "phone_number" })
  public phone_nummber: string;
  @column({ columnName: "fullname" })
  public fullname: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
