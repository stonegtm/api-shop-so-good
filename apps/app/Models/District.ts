import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class District extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "zip_code" })
  public zip_code: string;
  @column({ columnName: "name_th" })
  public name_th: string;
  @column({ columnName: "name_en" })
  public name_en: string;
  @column({ columnName: "amphure_id" })
  public amphure_id: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
