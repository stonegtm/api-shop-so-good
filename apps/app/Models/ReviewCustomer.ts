import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ReviewCustomer extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "title" })
  public title: string;
  @column({ columnName: "code_name" })
  public code_name: string;
  @column({ columnName: "title_sub" })
  public title_sub: string;
  @column({ columnName: "description" })
  public description: string;
  @column({ columnName: "image" })
  public image: string;
  @column({ columnName: "rating" })
  public rating: number;
  @column({ columnName: "status" })
  public status: boolean;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
