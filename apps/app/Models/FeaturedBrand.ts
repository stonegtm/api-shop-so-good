import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class FeaturedBrand extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "rating" })
  public rating: number;
  @column({ columnName: "status" })
  public status: boolean;
  @column({ columnName: "is_delete" })
  public is_delete: boolean;
  @column({ columnName: "image_1" })
  public image_1: string;
  @column({ columnName: "image_2" })
  public image_2: string;
  @column({ columnName: "index1" })
  public index1: number;
  @column({ columnName: "index2" })
  public index2: number;
  @column({ columnName: "alt" })
  public alt: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
