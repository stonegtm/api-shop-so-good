import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductOtherImage extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "product_other_id" })
  public product_other_id: number;
  @column({ columnName: "image" })
  public image: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
