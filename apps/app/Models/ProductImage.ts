import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductImage extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "image" })
  public image: string;
  @column({ columnName: "main" })
  public main: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}


