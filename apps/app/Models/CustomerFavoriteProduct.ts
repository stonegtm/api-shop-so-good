import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CustomerFavoriteProduct extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "status" })
  public status: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
