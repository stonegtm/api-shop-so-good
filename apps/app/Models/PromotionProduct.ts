import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class PromotionProduct extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "promotion_id" })
  public promotion_id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "product_other" })
  public product_other: string;
  @column({ columnName: "discount" })
  public discount: number;
  @column({ columnName: "status" })
  public status: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
