import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Province extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "code" })
  public code: string;
  @column({ columnName: "name_th" })
  public name_th: string;
  @column({ columnName: "name_en" })
  public name_en: string;
  @column({ columnName: "geography_id" })
  public geography_id: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
