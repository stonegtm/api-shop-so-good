import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Brand extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "name" })
  public name: string;
  @column({ columnName: "code_name" })
  public code_name: string;
  @column({ columnName: "status" })
  public status: boolean;
  @column({ columnName: "img" })
  public img: string;
  @column({ columnName: "meta_title" })
  public meta_title: string;
  @column({ columnName: "meta_description" })
  public meta_description: string;
  @column({ columnName: "meta_keywords" })
  public meta_keywords: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
