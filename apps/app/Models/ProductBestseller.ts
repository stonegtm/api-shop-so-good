import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductBestseller extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "rating" })
  public rating: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
