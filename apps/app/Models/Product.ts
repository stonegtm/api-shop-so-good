import { DateTime } from "luxon";
import { BaseModel, column, HasMany, hasMany, hasOne, HasOne } from "@ioc:Adonis/Lucid/Orm";
import ProductDetail from "App/Models/ProductDetail";
import ProductImage from "App/Models/ProductImage";
import ProductDescriptionImage from "App/Models/ProductDescriptionImage";
import ProductSeo from "App/Models/ProductSeo";
import ProductColor from "App/Models/ProductColor";
import ProductOther from "App/Models/ProductOther";
import ProductSize from "App/Models/ProductSize";
import ProductNewArrival from "App/Models/ProductNewArrival";
import PromotionProduct from "App/Models/PromotionProduct";
import Stock from "App/Models/Stock";



export default class Product extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "name" })
  public name: string;
  @column({ columnName: "code_name" })
  public code_name: string;
  @column({ columnName: "category" })
  public category: string;
  @column({ columnName: "brand_id" })
  public brand_id: number;
  @column({ columnName: "price" })
  public price: number;
  @column({ columnName: "discount" })
  public discount: number;
  @column({ columnName: "status" })
  public status: boolean;
  @column({ columnName: "is_delete" })
  public is_delete: boolean;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  // @hasOne(() => ProductDetail)
  
  @hasOne(() => ProductDetail, {
    foreignKey: 'product_id',
  })
  public productdetails: HasOne<typeof ProductDetail>

  @hasOne(() => Stock, {
    foreignKey: 'product_id',
  })
  public productstock: HasOne<typeof Stock>

  @hasMany(() => ProductImage, {
    foreignKey: 'product_id',
  })
  public productimage: HasMany<typeof ProductImage>

  @hasOne(() => ProductSeo, {
    foreignKey: 'product_id',
  })
  public productseo: HasOne<typeof ProductSeo> 

  @hasOne(() => ProductNewArrival, {
    foreignKey: 'product_id',
  })
  public productnewarrival: HasOne<typeof ProductNewArrival> 

  @hasOne(() => PromotionProduct, {
    foreignKey: 'product_id',
  })
  public promotionproduct: HasOne<typeof PromotionProduct> 

  @hasOne(() => ProductColor, {
    foreignKey: 'product_id',
  })
  public productColor: HasOne<typeof ProductColor> 

  @hasOne(() => ProductSize, {
    foreignKey: 'product_id',
  })
  public productSize: HasOne<typeof ProductSize> 

  @hasMany(() => ProductOther, {
    foreignKey: 'product_id',
  })
  public productOther: HasMany<typeof ProductOther>

  @hasMany(() => ProductDescriptionImage, {
    foreignKey: 'product_id',
  })
  public productDescriptionImage: HasMany<typeof ProductDescriptionImage>

  
}
// Product.$getRelation('productdetail')

