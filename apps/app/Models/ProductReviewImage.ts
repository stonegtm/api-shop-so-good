import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductReviewImage extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "product_review_id" })
  public product_review_id: number;
  @column({ columnName: "image_review" })
  public image_review: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
