import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductReview extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "review_description" })
  public review_description: string;
  @column({ columnName: "star" })
  public star: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
