import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class ProductNewArrival extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "newarrival_status" })
  public newarrival_status: boolean;
  @column({ columnName: "rating" })
  public rating: number;
  @column({ columnName: "is_delete" })
  public is_delete: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
