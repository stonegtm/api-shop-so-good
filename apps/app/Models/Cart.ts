import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Cart extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "product_other_id" })
  public product_other_id: number;
  @column({ columnName: "quantity" })
  public quantity: number;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
