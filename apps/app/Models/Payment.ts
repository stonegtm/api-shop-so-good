import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class Payment extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "customer_id" })
  public customer_id: number;
  @column({ columnName: "status" })
  public status: number;
  @column({ columnName: "cut_order" })
  public cut_order: number;
  @column({ columnName: "order_number" })
  public order_number: string;
  @column({ columnName: "product_order" })
  public product_order: string;
  @column({ columnName: "email" })
  public email: string;
  @column({ columnName: "address" })
  public address: string;
  @column({ columnName: "parish" })
  public parish: string;
  @column({ columnName: "district" })
  public district: string;
  @column({ columnName: "province" })
  public province: string;
  @column({ columnName: "zipcode" })
  public zipcode: string;
  @column({ columnName: "phone_number" })
  public phone_nummber: string;
  @column({ columnName: "fullname" })
  public fullname: string;
  @column({ columnName: "slip_image" })
  public slip_image: string;
  @column({ columnName: "price" })
  public price: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
