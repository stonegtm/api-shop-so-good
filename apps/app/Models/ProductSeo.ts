import { DateTime } from "luxon";
import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class ProductSeo extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "meta_title" })
  public meta_title: string;
  @column({ columnName: "meta_description" })
  public meta_description: string;
  @column({ columnName: "meta_keywords" })
  public meta_keywords: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
