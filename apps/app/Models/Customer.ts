import { DateTime } from "luxon";
import Hash from '@ioc:Adonis/Core/Hash'
import { BaseModel, column,beforeSave } from "@ioc:Adonis/Lucid/Orm";

export default class Customer extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "username" })
  public username: string;
  @column({serializeAs:null})
  public password: string;
  // @beforeSave()
  // public static async hashPassword(customer: Customer) {
  //   if (customer.$dirty.password) {
  //     customer.password = await Hash.make(customer.password)
  //   }
  // }
  @column({ columnName: "email" })
  public email: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
}
