import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class ProductSize extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "size" })
  public size: string;
  @column({ columnName: "code_size" })
  public code_size: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
