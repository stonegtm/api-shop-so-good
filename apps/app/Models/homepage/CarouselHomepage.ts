import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class CarouselHomepage extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "carousel_image" })
  public carousel_image: string;
  @column({ columnName: "rating" })
  public rating: number;
  @column({ columnName: "carousel_alt" })
  public carousel_alt: string;
  @column({ columnName: "status" })
  public status: boolean;
  @column({ columnName: "is_delete" })
  public is_delete: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
