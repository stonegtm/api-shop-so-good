import { DateTime } from "luxon";
import {
  BaseModel,
  column,
  HasMany,
  hasMany,
  hasOne,
  HasOne,
  manyToMany,
  ManyToMany,
} from "@ioc:Adonis/Lucid/Orm";
import CategoryList from "App/Models/CategoryList";

export default class CategoryMain extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "name" })
  public name: string;
  @column({ columnName: "code_name" })
  public code_name: string;
  @column({ columnName: "displayed" })
  public displayed: boolean;
  @column({ columnName: "main" })
  public main: boolean;
  @column({ columnName: "img_cover" })
  public img_cover: string;
  @column({ columnName: "meta_title" })
  public meta_title: string;
  @column({ columnName: "meta_description" })
  public meta_description: string;
  @column({ columnName: "meta_keywords" })
  public meta_keywords: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @hasMany(() => CategoryList, {
    foreignKey: "id_category_main",
  })
  public categorylist: HasMany<typeof CategoryList>;

  @manyToMany(() => CategoryList, {
    localKey: "id",
    pivotForeignKey: "id",
    relatedKey: "id",
    pivotRelatedForeignKey: "id_category_main",
  })
  public skills: ManyToMany<typeof CategoryList>;
}
