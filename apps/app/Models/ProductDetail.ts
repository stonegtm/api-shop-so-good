import { DateTime } from "luxon";
import { BaseModel, column,belongsTo } from "@ioc:Adonis/Lucid/Orm";
import Product from "App/Models/Product";

export default class ProductDetail extends BaseModel {
  @column({ isPrimary: true })
  public id: number;
  @column({ columnName: "product_id" })
  public product_id: number;
  @column({ columnName: "description" })
  public description: string;
  @column({ columnName: "content" })
  public content: string;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;
  
}
