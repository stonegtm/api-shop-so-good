import { DateTime } from 'luxon'
import { BaseModel, column } from '@ioc:Adonis/Lucid/Orm'

export default class Promotion extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: "image" })
  public image: string;
  @column({ columnName: "banner_pc" })
  public banner_pc: string;
  @column({ columnName: "banner_mobile" })
  public banner_mobile: string;
  @column({ columnName: "name" })
  public name: string;
  @column({ columnName: "code_name" })
  public code_name: string;
  @column({ columnName: "status" })
  public status: boolean;
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
}
