import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
import YouMayAlsoLike from "App/Models/YouMayAlsoLike";
("use strict");
import Application from "@ioc:Adonis/Core/Application";

export default class YouMayAlsoLikesController {
    public async index(ctx: HttpContextContract) {
        const res = await Database.from('you_may_also_likes as a')
            .join('products as b', 'a.product_id', 'b.id')
            .join('product_images as c', 'a.product_id', 'c.product_id')
            .where('c.main', 1)
        return { success: true, data: res }
    }

    public async youMayAlsoLikePickedFront(ctx: HttpContextContract) {
        const res = await Database.from('you_may_also_likes as a')
            .join('products as b', 'a.product_id', 'b.id')
            .where("b.status", 1)
            .join("product_details as c", "a.product_id", "c.product_id")
            .join("product_images as d", "a.product_id", "d.product_id")
            .join("product_new_arrivals as e", "a.product_id", "e.product_id")
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select("b.*", "c.description", "d.*", 'e.*',"g.discount as discount_promotion")
        const pdo = await Database.from('product_others')
        return { success: true, data: res, product_other: pdo }
    }

    public async addYouMayAlsoLike(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const check = await Database.from('you_may_also_likes').where('product_id', data.product_id).first()
        // console.log(check);
        if (check) {
            return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
        } else {
            const res = await YouMayAlsoLike.create(data)
            if (res) {
                return { success: true, message: 'เพิ่มเรียบร้อย' }
            } else {
                return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
            }
        }
    }
    public async deleteYouMayAlsoLike(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await YouMayAlsoLike.query().where('product_id', data.product_id).delete()
        if (res) {
            return { success: true, message: 'ลบสำเร็จแล้ว' }
        } else {
            return { success: false, message: 'ลบไม่สำเร็จแล้ว' }
        }
    }


}
