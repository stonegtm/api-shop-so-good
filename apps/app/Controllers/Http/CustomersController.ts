import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
("use strict");
import Hash from "@ioc:Adonis/Core/Hash";
import Customer from "App/Models/Customer";
import CustomerFavoriteProduct from "App/Models/CustomerFavoriteProduct";
import CustomerAddress from "App/Models/CustomerAddress";
import CustomerProfile from "App/Models/CustomerProfile";
import Database from "@ioc:Adonis/Lucid/Database";
import { schema, rules } from "@ioc:Adonis/Core/Validator";
import CustomerProfiles from "Database/migrations/1639598862047_customer_profiles";
import CustomerAddresses from "Database/migrations/1637703268310_customer_addresses";
export default class CustomersController {
  public async customerData(ctx: HttpContextContract) {
    const res = await Customer.query();
    return res;
  }
  public async checkRegister(ctx: HttpContextContract) {
    const data = ctx.request.all()
    if (data) {
      const res = await Database.from('customers').where('email', data.email).first()
      if (res) {
        return { facebook: true }
      } else {
        const dataCustomer = {
          username: data.name,
          password: await Hash.make(data.id),
          email: data.email,
        };
        const register = await Customer.create(dataCustomer);
        return { facebook: true }
      }
    }
  }


  public async createCustomer(ctx: HttpContextContract) {
    // console.log(ctx.request.all());

    const customer = schema.create({
      username: schema.string({ trim: true }),
      password: schema.string({ trim: true }, [
        rules.confirmed("passwordConfirmation"),
      ]),
      email: schema.string({ trim: true }, [
        rules.email(),
        rules.maxLength(255),
        rules.unique({ table: "customers", column: "email" }),
      ]),
    });

    const messages = {
      maxLength: "{{field}} must ...",
      confirmed: "Password ไม่ตรงกันกรุณาใส่ Password ให้ตรงกันด้วยค่ะ",
      email: "รูปแบบของ Email ไม่ถูกต้องกรุณาใส่ให้ถูกต้อง",
      "email.unique": "Email นี้มีการใช้งานแล้ว",
    };
    const data = await ctx.request.validate({ schema: customer, messages });
    if (data) {
      const dataCustomer = {
        username: data.username,
        password: await Hash.make(data.password),
        email: data.email,
      };
      const res = await Customer.create(dataCustomer);
      if (res) {
        return { success: true, message: "สมัครสมาชิกสำเร็จแล้ว" };
      } else {
        return { success: false, message: "สมัครสมาชิกไม่สำเร็จ" };
      }
    }
    return data;
  }
  public async updateProfile(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const tableCustomerProfile = await Database.from("customer_profiles")
      .where("customer_id", data.customer_id)
      .first();
    if (tableCustomerProfile) {
      const res = await CustomerProfile.query()
        .where("customer_id", data.customer_id)
        .update({
          fullname: data.fullname,
          phone_number: data.phone_number,
          sex: data.sex,
          day: data.day,
          month: data.month,
          year: data.year,
        });
      if (res) {
        return { success: true, data: res, message: "บันทึกข้อมูลเรียบร้อย" };
      } else {
        return { success: false, message: "ไม่มีข้อมูลดังกล่าว" };
      }
    } else {
      const res = await CustomerProfile.create(data);
      if (res) {
        return { success: true, data: res, message: "บันทึกข้อมูลเรียบร้อย" };
      } else {
        return { success: false, message: "ไม่มีข้อมูลดังกล่าว" };
      }
    }
  }
  public async addNewAdress(ctx: HttpContextContract) {
    // const data = ctx.request.all();
    const address = schema.create({
      customer_id: schema.number(),
      address: schema.string({ trim: true }),
      district: schema.string({ trim: true }),
      fullname: schema.string({ trim: true }),
      parish: schema.string({ trim: true }),
      phone_number: schema.string({ trim: true }),
      province: schema.string({ trim: true }),
      zipcode: schema.string({ trim: true }),
    });
    const data = await ctx.request.validate({ schema: address });

    const res = await CustomerAddress.create(data);
    if (res) {
      return { success: true, data: res, message: "บันทึกข้อมูลเรียบร้อย" };
    } else {
      return { success: false, message: "ไม่มีข้อมูลดังกล่าว" };
    }
  }
  public async updateAdress(ctx: HttpContextContract) {
    const address = schema.create({
      id: schema.number(),
      address: schema.string({ trim: true }),
      district: schema.string({ trim: true }),
      fullname: schema.string({ trim: true }),
      parish: schema.string({ trim: true }),
      phone_number: schema.string({ trim: true }),
      province: schema.string({ trim: true }),
      zipcode: schema.string({ trim: true }),
    });
    const data = await ctx.request.validate({ schema: address });
    const res = await CustomerAddress.query().where("id", data.id).update({
      address: data.address,
      district: data.district,
      fullname: data.fullname,
      parish: data.parish,
      phone_number: data.phone_number,
      province: data.province,
      zipcode: data.zipcode,
    });
    if (res) {
      return { success: true, data: res, message: "บันทึกสำเร็จแล้ว" };
    } else {
      return { success: false, message: "บันทึกสำเร็จไม่สำเร็จ" };
    }
  }
  public async getAdressById(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await CustomerAddress.query().where("id", data.id).first();
    return { success: true, data: res, message: "เรียกข้อมูลสำเร็จ" };
  }
  public async getAdress(ctx: HttpContextContract) {
    const { id } = ctx.auth.user?.toJSON();
    const res = await CustomerAddress.query().where("customer_id", id);
    return { success: true, data: res, message: "เรียกข้อมูลสำเร็จ" };
  }

  public async getProfile(ctx: HttpContextContract) {
    const { id } = ctx.auth.user?.toJSON();
    const res = await CustomerProfile.query().where("customer_id", id);

    if (res.length >= 1) {
      return { success: true, data: res, message: "เรียกข้อมูลสำเร็จ" };
    } else {
      return { success: false, message: "ไม่มีข้อมูลดังกล่าว" };
    }
  }

  public async loginCustomer(ctx: HttpContextContract) {
    // console.log(ctx.request.all());
    const email = ctx.request.input("email");
    const password = ctx.request.input("password");
    const token = await ctx.auth.use("api").attempt(email, password, {
      expiresIn: "7days",
    });
    // console.log(token);
    return { success: true, user: token.toJSON() };
  }
  public async loginFacebook(ctx: HttpContextContract) {
    // console.log(ctx.request.all());
    const res = await Database.from('customers').where('email', ctx.request.all().email).first()
    if (!res) {
      const tableCustommer = await Customer.create({
        email: ctx.request.all().email,
        password: await Hash.make(ctx.request.all().fb_token),
        username: ctx.request.all().name,
      })
      if (tableCustommer) {
        const profile = await CustomerProfile.create({
          customer_id: tableCustommer.id,
          fullname: ctx.request.all().name,
          day: '31',
          month: '12',
          year: '2020',
          sex: 1,
          phone_number: '0000000000'
        })
        if (profile) {
          const token = await ctx.auth.use("api").attempt(ctx.request.all().email, ctx.request.all().fb_token, {
            expiresIn: "7days",
          });
          if (token) {
            return { success: true, user: token.toJSON() };
          } else {
            return { success: false, message: 'อีเมลล์นี้ใช้สมัครแบบปกติไปแล้ว' };
          }
        }
      }
    } else {
      const token = await ctx.auth.use("api").attempt(ctx.request.all().email, ctx.request.all().fb_token, {
        expiresIn: "7days",
      });
      return { success: true, user: token.toJSON() };
    }

  }


  public async logoutCustomer(ctx: HttpContextContract) {
    await ctx.auth.use("api").logout();
    return { success: true, message: "Logout Success!!!" };
  }

  public async getUsername(ctx: HttpContextContract) {
    const data = ctx.auth.user;
    // const favorite = await Database.from('customer_favorite_products').where('customer_id', data?.toJSON().id)
    // console.log(favorite);
    return { suscess: true, data: { data: data?.toJSON() } };
  }

  public async getCart(ctx: HttpContextContract) {
    const { id } = ctx.auth.user?.toJSON();
    if (id) {
      const res = await Database.from("carts as a")
        .where("a.customer_id", id)
        .join("products as b", "a.product_id", "b.id")
        .join("product_details as c", "a.product_id", "c.product_id")
        .leftJoin("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
        .join("product_new_arrivals as e", "a.product_id", "e.product_id")
        .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
        .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
        .select(
          "f.price as price_other_product",
          "f.discount as discount_other_product",
          "g.discount as discount_promotion",
          "g.product_other as discount_product_other",
          "a.*",
          "a.id as cart_id",
          "b.*",
          "b.id",
          "c.*",
          "d.*",
          "e.newarrival_status",
        );
      // const image = await Database.from('carts as a').where('a.customer_id', id)
      //   .join("products as b", "a.product_id", "b.id")
      //   .leftJoin("product_images as d", "a.product_id", "d.product_id")
      //   .select('d.*')

      // for (let i = 0; i < image.length; i++) {
      //   for (let o = 0; o < res.length; i++) {
      //     // console.log(res[o].product_id,'fffff',image[i].product_id);

      //     if (res[o].product_id === image[i].product_id) {
      //         console.log(res[o]);

      //         // res[o].image = image[i].image
      //     }
      //   }
      // }
      // console.log(res);



      // const data = [...new Set(res)];


      // console.log(JSON.parse(res.discount_product_other));
      if (res) {
        for (let i = 0; i < res.length; i++) {
          res[i].colorAndSize = []
          if (res[i].product_other_id) {
            let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
            res[i].colorAndSize.push(coAndSi)
          }
        }
        // JSON.parse(res[1].discount_product_other)
        if (res) {
          for (let i = 0; i < res.length; i++) {
            if (res[i].discount_product_other) {
              const parse = JSON.parse(res[i].discount_product_other)
              // console.log(parse.length);
              for (let b = 0; b < parse.length; b++) {
                if (res[i].product_other_id == parse[b].product_other_id) {
                  res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                }
              }
            }

          }
        }
        // console.log(res);

        const favorite = await Database.from('customer_favorite_products').where('customer_id', id)
        if (favorite) {
          return { data: res, favorite: favorite }
        } else {
          return { data: res, favorite: null }
        }
      } else {
        return false
      }
    }

  }
  public async addressCustomer(ctx: HttpContextContract) {
    const { id } = ctx.request.params();
    const res = await CustomerAddress.query().where("customer_id", id)
    if (res) {
      return { success: true, data: res, message: "ดึงข้อมูลได้สำเร็จแล้ว" };
    } else {
      return { success: false, message: "ลูกค้ายังไม่ได้กรอกข้อมูล" };
    }
  }
  public async getProfileSingle(ctx: HttpContextContract) {
    const data = ctx.request.all();

    const res = await CustomerProfile.query()
      .where("customer_id", data.id)
      .first();
    if (res) {
      return { success: true, data: res, message: "ดึงข้อมูลได้สำเร็จแล้ว" };
    } else {
      return { success: false, message: "ลูกค้ายังไม่ได้กรอกข้อมูล" };
    }
  }
  public async updownQuantity(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const { id } = ctx.auth.user?.toJSON();
    const update = await Database.from("carts").where("id", data.id).update({
      quantity: data.quantity,
    });
    if (!update) {
      return { success: false, message: "เพิ่มสิ้นค้าสำเร็จ" };
    }
    const res = await Database.from("carts as a")
      .where("customer_id", id)
      .join("products as b", "a.product_id", "b.id")
      .join("product_details as c", "a.product_id", "c.product_id")
      .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
      .join("product_new_arrivals as e", "a.product_id", "e.product_id")
      .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
      .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
      .select(
        "f.price as price_other_product",
        "g.discount as discount_promotion",
        "f.discount as discount_other_product",
        "g.product_other as discount_product_other",
        "a.*",
        "a.id as cart_id",
        "b.*",
        "c.*",
        "d.*",
        "e.newarrival_status"
      );
    for (let i = 0; i < res.length; i++) {
      res[i].colorAndSize = []
      if (res[i].product_other_id) {
        let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
        res[i].colorAndSize.push(coAndSi)
      }
    }
    if (res) {
      for (let i = 0; i < res.length; i++) {
        if (res[i].discount_product_other) {
          const parse = JSON.parse(res[i].discount_product_other)
          // console.log(parse.length);
          for (let b = 0; b < parse.length; b++) {
            if (res[i].product_other_id == parse[b].product_other_id) {
              res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
            }
          }
        }

      }
    }
    return { success: true, data: res };
  }

  public async deleteCart(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await Database.from("carts").where("id", data.cart_id).delete();
    const { id } = ctx.auth.user?.toJSON();
    if (res) {
      const dataReturn = await Database.from("carts as a")
        .where("customer_id", id)
        .join("products as b", "a.product_id", "b.id")
        .join("product_details as c", "a.product_id", "c.product_id")
        .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
        .join("product_new_arrivals as e", "a.product_id", "e.product_id")
        .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
        .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
        .select(
          "g.discount as discount_promotion",
          "g.product_other as discount_product_other",
          "f.price as price_other_product",
          "f.discount as discount_other_product",
          "a.*",
          "a.id as cart_id",
          "b.*",
          "c.*",
          "d.*",
          "e.newarrival_status"
        );
      for (let i = 0; i < dataReturn.length; i++) {
        dataReturn[i].colorAndSize = []
        if (dataReturn[i].product_other_id) {
          let coAndSi = await Database.from('product_others').where('id', dataReturn[i].product_other_id).first()
          dataReturn[i].colorAndSize.push(coAndSi)
        }
      }
      // console.log(dataReturn.length);

      if (dataReturn) {
        for (let i = 0; i < dataReturn.length; i++) {
          if (dataReturn[i].discount_product_other) {
            const parse = JSON.parse(dataReturn[i].discount_product_other)
            for (let b = 0; b < parse.length; b++) {
              if (dataReturn[i].product_other_id == parse[b].product_other_id) {
                dataReturn[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
              }
            }
          }
        }
      }
      return { success: true, data: dataReturn };
    } else {
      const dataReturn = await Database.from("carts as a")
        .where("customer_id", id)
        .join("products as b", "a.product_id", "b.id")
        .join("product_details as c", "a.product_id", "c.product_id")
        .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
        .join("product_new_arrivals as e", "a.product_id", "e.product_id")
        .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
        .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
        .select(
          "g.discount as discount_promotion",
          "f.price as price_other_product",
          "f.discount as discount_other_product",
          "g.product_other as discount_product_other",
          "a.*",
          "a.id as cart_id",
          "b.*",
          "c.*",
          "d.*",
          "e.newarrival_status"
        );
      for (let i = 0; i < dataReturn.length; i++) {
        dataReturn[i].colorAndSize = []
        if (dataReturn[i].product_other_id) {
          let coAndSi = await Database.from('product_others').where('id', dataReturn[i].product_other_id).first()
          dataReturn[i].colorAndSize.push(coAndSi)
        }
      }
      if (dataReturn) {
        for (let i = 0; i < dataReturn.length; i++) {
          if (dataReturn[i].discount_product_other) {
            const parse = JSON.parse(dataReturn[i].discount_product_other)
            for (let b = 0; b < parse.length; b++) {
              if (dataReturn[i].product_other_id == parse[b].product_other_id) {
                dataReturn[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
              }
            }
          }
        }
      }
      return { success: false, data: dataReturn };
    }
  }


  public async favoriteProduct(ctx: HttpContextContract) {
    const data = ctx.request.all()
    const tableFavorite = await Database.from('customer_favorite_products')
      .where('customer_id', data.customer_id)
      .where('product_id', data.product_id).first()
    // console.log(tableFavorite);
    if (tableFavorite) {
      const res = await CustomerFavoriteProduct.query()
        .where('customer_id', data.customer_id)
        .where('product_id', data.product_id).delete()
      const tableFavorite = await Database.from('customer_favorite_products').where('customer_id', data.customer_id)
      return { success: true, data: tableFavorite, message: 'ติดตามสินค้าแล้ว' }
    } else {
      const res = await CustomerFavoriteProduct.create({
        customer_id: data.customer_id,
        product_id: data.product_id,
        status: true,
      })
      const tableFavorite = await Database.from('customer_favorite_products').where('customer_id', data.customer_id)
      return { success: true, data: tableFavorite, message: 'ติดตามสินค้าแล้ว' }
    }
  }
  public async favoriteProductPicked(ctx: HttpContextContract) {
    // const { id } = ctx.auth.user?.toJSON();
    const id = ctx.request.all().customer_id
    const tableFavorite = await Database.from('customer_favorite_products')
      .where('customer_id', id)
    return tableFavorite
  }

}
