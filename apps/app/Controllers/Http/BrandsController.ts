import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
("use strict");
import Brand from "App/Models/Brand";

export default class BrandsController {
  public async imageForUpload(coverImage) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const addBeforeNameImg = new Date().getTime();

    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + coverImage.data.clientName;
    // อัพรูปลงเครื่อง api
    await coverImage.move(Application.tmpPath("uploads/brands"), {
      name: newNameImg,
      overwrite: true,
    });
    return { success: true, message: newNameImg };
  }

  public async selectImageBrands(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/brands/" + name)
    );
    ctx.response.stream(getStream);
  }
  //โชว์รูปของ Brands

  public async index(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("img");
    const regex = /\s/g;

    let data = {
      name: ctx.request.all().name,
      code_name: ctx.request.all().name.replace(regex, "_"),
      status: ctx.request.all().status,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
      img:''
    };
    // console.log(data);

    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ เริ่ม
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ จบ
    const res = await Brand.create(data);
    if (res) {
      return { success: true, message: "สร้าง Brands สำเร็จ" };
    } else {
      return { success: false, message: "สร้าง Brands ไม่สำเร็จ" };
    }
  }
  public async selectBrands(ctx: HttpContextContract) {
    const res = await Brand.query().select();
    return res;
  }

  public async editBrands(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("newImg");
    let data = {
      id: ctx.request.all().id,
      name: ctx.request.all().name,
      status: ctx.request.all().status,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
      img_cover:''
    };
    const delImg = ctx.request.only(["img"]);

    if (delImg.img != 'undefined' && delImg.img != 1) {
      await fs.unlink(
        Application.tmpPath("uploads/brands/") + delImg.img,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img_cover = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ

    const res = await Brand.query().where("id", data.id).update({
      name: data.name,
      status: data.status,
      img: data.img_cover,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
    });
    if (res) {
      return { success: true, message: "แก้ไข้สถานะสำเร็จ" };
    } else {
      return { success: false, message: "แก้ไข้สถานะไม่สำเร็จ" };
    }
  }

  //DELETE BRANDS START
  public async deleteBrands(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(img_cover);

    if (data.img) {
      await fs.unlink(
        Application.tmpPath("uploads/brands/") + data.img,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }
    const res = await Brand.query().where("id", data.id).delete();
    if (res) {
      return { success: true, message: "ลบสำเร็จแล้ว" };
    } else {
      return { success: false, message: "ลบไม่สำเร็จแล้ว" };
    }
  }

  public async selectOneById(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await Brand.query().where("id", data.id).select();
    return res;
  }

  //SELECT
  public async selectAll(ctx: HttpContextContract) {
    const res = await Brand.query().select()
    return res
  }
}
