import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
("use strict");
import Hash from "@ioc:Adonis/Core/Hash";
import Employee from "App/Models/Employee";
import { schema, rules } from "@ioc:Adonis/Core/Validator";

export default class EmployeesController {
  public async createEmployee(ctx: HttpContextContract) {
    const employee = schema.create({
      username: schema.string({ trim: true }),
      password: schema.string({ trim: true }, [
        rules.confirmed("passwordConfirmation"),
      ]),
    });

    const messages = {
      confirmed: "Password ไม่ตรงกันกรุณาใส่ Password ให้ตรงกันด้วยค่ะ",
    };
    const data = await ctx.request.validate({ schema: employee, messages });
    if (data) {
      const dataEmployee = {
        username: data.username,
        password: await Hash.make(data.password),
      };
      const res = await Employee.create(dataEmployee);
      if (res) {
        return { success: true, message: "สมัครสมาชิกสำเร็จแล้ว" };
      } else {
        return { success: false, message: "สมัครสมาชิกไม่สำเร็จ" };
      }
    }
    return data;
  }

  public async loginEmployee(ctx: HttpContextContract) {
    //   console.log(ctx.request.all);

    const username = ctx.request.input("username");
    const password = ctx.request.input("password");
    const token = await ctx.auth
      .use("api_employee")
      .attempt(username, password, {
        expiresIn: "7days",
      });

    return { success: true, data: token.toJSON() };
  }
  public async logoutEmployee(ctx: HttpContextContract) {
    await ctx.auth.use("api_employee").logout();
    return { success: true, message: "Logout Success!!!" };
  }
  public async getEmployee(ctx: HttpContextContract) {
    const data = ctx.auth.user;
    return { suscess: true, data: data?.toJSON() };
  }
}
