import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
("use strict");
import CategoryList from "App/Models/CategoryList";
import Database from "@ioc:Adonis/Lucid/Database";

export default class CategoryListsController {
  public async imageForUpload(coverImage) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const addBeforeNameImg = new Date().getTime();

    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + coverImage.data.clientName;
    // อัพรูปลงเครื่อง api
    await coverImage.move(
      Application.tmpPath("uploads/categories/list/cover"),
      {
        name: newNameImg,
        overwrite: true,
      }
    );
    return { success: true, message: newNameImg };
  }
  //โชว์รูปของ Categories หลัก
  public async selectImageList(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/categories/list/cover/" + name)
    );
    ctx.response.stream(getStream);
  }
  //โชว์รูปของ Categories หลัก

  public async index(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("img_cover");
    let data = {
      name: ctx.request.all().name,
      displayed: ctx.request.all().displayed,
      main: ctx.request.all().main,
      id_category_main: ctx.request.all().id_category_main,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
    };
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ เริ่ม
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img_cover = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ จบ

    const res = await CategoryList.create(data);
    if (res) {
      return { success: true, message: "สร้าง CategoryList สำเร็จ" };
    } else {
      return { success: false, message: "สร้าง CategoryList ไม่สำเร็จ" };
    }
  }

  //Selete categories list
  public async selectCategories(ctx: HttpContextContract) {
    const data = ctx.request.all();
    //  console.log(data);
    const res = await CategoryList.query()
      .where("id_category_main", data.id_category_main)
      .select();
    return res;
  }

  //EDIT CATEGORIES LIST
  public async editCategoriesList(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("newImg");
    let data = {
      id: ctx.request.all().id,
      name: ctx.request.all().name,
      displayed: ctx.request.all().displayed,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
    };
    const delImg = ctx.request.only(["img_cover"]);
    if (delImg.img_cover != "undefined" && delImg.img_cover != 1) {
      await fs.unlink(
        Application.tmpPath("uploads/categories/cover/") + delImg.img_cover,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }

    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img_cover = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ

    const res = await CategoryList.query().where("id", data.id).update({
      name: data.name,
      displayed: data.displayed,
      img_cover: data.img_cover,
      meta_title: data.meta_title,
      meta_description: data.meta_description,
      meta_keywords: data.meta_keywords,
    });
    if (res) {
      return { success: true, message: "แก้ไขสำเร็จแล้ว" };
    } else {
      return { success: false, message: "แก้ไขไม่สำเร็จแล้ว" };
    }
  }

  public async selectByid(ctx: HttpContextContract) {
    const id = ctx.request.all().id;
    const res = await CategoryList.query().where("id", id).select();
    if (res) {
      return res;
    } else {
      return { success: false, message: "ไม่สามารถเปิดได้ในตอนนี้" };
    }
  }
  public async deleteCategoriesList(ctx: HttpContextContract) {
    // const id = ctx.request.all().id;
    const data = ctx.request.all();

    if (data.img_cover) {
      // console.log('inside');

      await fs.unlink(
        Application.tmpPath("uploads/categories/list/cover/") + data.img_cover,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }
    const res = await CategoryList.query().where("id", data.id).delete();
    if (res) {
      return { success: true, message: "ลบสำเร็จแล้ว" };
    } else {
      return { success: false, message: "ลบไม่สำเร็จแล้ว" };
    }
  }
  //
  //
  //
  //
  //
  public async selectJoinCategories(ctx: HttpContextContract) {
    const res = await Database.from("pd_categories as a")
      .join("category_lists as b", "a.name", "b.id_category_main")
      .select("b.name", "b.id_category_main", "b.id");
    return res;
  }

  public async selectCategoryByName(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(data);
    
    const res = await CategoryList.query()
      .where("id_category_main", data.id)
      .select();
    return res;
  }
}
