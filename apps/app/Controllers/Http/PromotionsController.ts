import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
import Promotion from "App/Models/Promotion";
import PromotionProduct from "App/Models/PromotionProduct";

export default class PromotionsController {
    public async imageForUpload(coverImage) {
        //เพิ่มวันที่ต่อหน้าชื่อ
        const addBeforeNameImg = new Date().getTime();

        //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
        const newNameImg = 'promotion_' + coverImage.data.clientName
        // อัพรูปลงเครื่อง api
        await coverImage.move(Application.tmpPath("uploads/promotion/cover"), {
            name: newNameImg,
            overwrite: true,
        });
        return { success: true, message: newNameImg };
    }
    public async imageForUploadVersionPc(coverImage) {
        //เพิ่มวันที่ต่อหน้าชื่อ
        const addBeforeNameImg = new Date().getTime();

        //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
        const newNameImg = 'promotion_banner_' + coverImage.data.clientName
        // อัพรูปลงเครื่อง api
        await coverImage.move(Application.tmpPath("uploads/promotion/banner_pc"), {
            name: newNameImg,
            overwrite: true,
        });
        return { success: true, message: newNameImg };
    }
    public async imageForUploadVersionMobile(coverImage) {
        //เพิ่มวันที่ต่อหน้าชื่อ
        const addBeforeNameImg = new Date().getTime();

        //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
        const newNameImg = 'promotion_banner_' + coverImage.data.clientName
        // อัพรูปลงเครื่อง api
        await coverImage.move(Application.tmpPath("uploads/promotion/banner_mobile"), {
            name: newNameImg,
            overwrite: true,
        });
        return { success: true, message: newNameImg };
    }
    //โชว์รูปของ Promotion หลัก
    public async showImagePromotion(ctx: HttpContextContract) {
        //เก็บพารามมาจากหน้า
        const { name } = ctx.request.params();
        //เรียกรูปภาพออกไปโชว์
        const getStream = await Drive.getStream(
            Application.tmpPath("uploads/promotion/cover/" + name)
        );
        ctx.response.stream(getStream);
    }
    public async showImageBannerPcPromotion(ctx: HttpContextContract) {
        //เก็บพารามมาจากหน้า
        const { name } = ctx.request.params();
        //เรียกรูปภาพออกไปโชว์
        const getStream = await Drive.getStream(
            Application.tmpPath("uploads/promotion/banner_pc/" + name)
        );
        ctx.response.stream(getStream);
    }
    public async showImageBannerMobilePromotion(ctx: HttpContextContract) {
        //เก็บพารามมาจากหน้า
        const { name } = ctx.request.params();
        //เรียกรูปภาพออกไปโชว์
        const getStream = await Drive.getStream(
            Application.tmpPath("uploads/promotion/banner_mobile/" + name)
        );
        ctx.response.stream(getStream);
    }
    //โชว์รูปของ Promotion end

    public async showPromotionSingleBackend(ctx: HttpContextContract) {
        const { code_name } = ctx.request.params()

        const data = await Database.from('promotions').where('id', code_name).first()

        return data
    }
    public async showPromotionSingleFront(ctx: HttpContextContract) {
        const { code_name } = ctx.request.params()

        const data = await Database.from('promotions').where('code_name', code_name).first()
        return data
    }
    public async showBackend(ctx: HttpContextContract) {
        const data = Promotion.query()
        return data
    }
    public async showPromotionFront(ctx: HttpContextContract) {
        const data = Promotion.query()
        return data
    }
    public async addPromotion(ctx: HttpContextContract) {
        const coverImage = ctx.request.file("image");
        const bannerPc = ctx.request.file("banner_pc");
        const bannerMobile = ctx.request.file("banner_mobile");
        const regex = /\s/g;
        let data = {
            name: ctx.request.all().name,
            code_name: ctx.request.all().name.replace(regex, "_"),
            status: true,
        };
        //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
        if (coverImage && bannerPc && bannerMobile) {
            const waiting = await this.imageForUpload(coverImage);
            const waitingBannerPc = await this.imageForUploadVersionPc(bannerPc);
            const waitingBannerMobile = await this.imageForUploadVersionMobile(bannerMobile);
            if (waiting.success && waitingBannerPc.success && waitingBannerMobile.success) {
                const res = await Promotion.create({
                    name: data.name,
                    code_name: data.code_name,
                    status: data.status,
                    image: waiting.message,
                    banner_pc: waitingBannerPc.message,
                    banner_mobile: waitingBannerMobile.message
                });
                if (res) {
                    return { success: true, message: "สร้าง Promotion สำเร็จ" };
                } else {
                    return { success: true, message: "สร้าง Promotion ไม่สำเร็จ" };
                }
            }
        }
        //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
    }
    public async addProductPromotion(ctx: HttpContextContract) {
        const data = ctx.request.all()
        if (data.search) {
            const check = await Database.from('promotion_products').where('promotion_id', data.data.promotion_id).where('product_id', data.data.product_id).first()
            if (check) {
                return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
            } else {
                const res = await PromotionProduct.create({
                    promotion_id: data.data.promotion_id,
                    product_id: data.data.product_id,
                    discount: 0,
                    status: true
                })
                if (res) {
                    return { success: true, message: 'เพิ่มเรียบร้อย' }
                } else {
                    return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
                }
            }
        } else if (data.select) {
            const check = await Database.from('promotion_products').where('promotion_id', data.data.promotion_id).where('product_id', data.data.product_id).first()
            // console.log(check);
            if (check) {
                return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
            } else {
                const res = await PromotionProduct.create({
                    promotion_id: data.data.promotion_id,
                    product_id: data.data.product_id,
                    discount: 0,
                    status: true
                })
                if (res) {
                    return { success: true, message: 'เพิ่มเรียบร้อย' }
                } else {
                    return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
                }
            }
        }
    }
    public async getProductPromotionPicked(ctx: HttpContextContract) {
        const promotion_id = ctx.request.all().promotion_id
        const res = await Database.from('promotion_products as a').where('a.promotion_id', promotion_id)
            .join('products as b', 'a.product_id', 'b.id')
            .join('product_images as c', 'a.product_id', 'c.product_id').where('c.main', 1)
            .select('a.discount as discountPromotion', 'a.id as id_product_promotion', 'b.*', 'c.*')
        const pdo = await Database.from('product_others')
        return { success: true, data: res, product_other: pdo }
    }

    public async editDiscountProductPromotion(ctx: HttpContextContract) {
        const data = ctx.request.all()
        // console.log(data);
        const res = await PromotionProduct.query().where('id', data.id).update({ discount: data.discount })
        if (res) {
            return { success: true, message: 'แก้ไขสำเร็จ' }
        } else {
            return { success: false, message: 'แก้ไขไม่สำเร็จ' }
        }
    }
    public async deleteProductPromotion(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await PromotionProduct.query().where('id', data.id).delete()
        if (res) {
            return { success: true, message: 'ลบสำเร็จ' }
        } else {
            return { success: false, message: 'ลบไม่สำเร็จ' }
        }
    }
    public async showPromotionProduct(ctx: HttpContextContract) {
        const data = ctx.request.all()
        // console.log(data);

        const tableProduct = await Database.from('promotions as a').where('a.status', 1).where('a.code_name', data.code_name)
            .join('promotion_products as bb', 'bb.promotion_id', 'a.id')
            .join('products as aa', 'bb.product_id', 'aa.id').where('aa.status', 1)
            .join('product_images as b', 'bb.product_id', 'b.product_id').where('b.main', 1)
            .join('product_details as c', 'bb.product_id', 'c.product_id')
            .join('product_new_arrivals as d', 'bb.product_id', 'd.product_id')
            .join('product_seos as e', 'bb.product_id', 'e.product_id')
            .join('stocks as f', 'bb.product_id', 'f.product_id')
            .select('bb.discount as discount_promotion', 'bb.product_other as discount_other', 'aa.*', 'b.*', 'c.*', 'd.*', 'e.*', 'f.*')
        const tableProductOther = await Database.from('product_others as a')
        // .leftJoin('promotion_products as b', 'a.product_id', 'a.product_id')
        //     .select('a.*', 'b.product_other')
        // tableProductOther.map(edit => {
        //     const parse = JSON.parse(edit.product_other)
        //     edit.discount_promotion_other = null
        //     for (let i = 0; i < parse.length; i++) {
        //         if (edit.id == parse[i].product_other_id) {
        //             edit.discount_promotion_other = parse[i].discount_other_promotion
        //         }
        //     }
        // })
        tableProduct.map(edit => {
            edit.pdo = []
            for (let i = 0; i < tableProductOther.length; i++) {
                if (edit.id == tableProductOther[i].product_id) {
                    edit.pdo.push(tableProductOther[i])
                }
            }
        })
        if (tableProduct.length >= 1) {
            return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
        } else {
            return { success: false, message: 'ดึงข้อมูลได้!!!' }
        }
    }

    public async nameProductForAdd(ctx: HttpContextContract) {
        const res = await Database.from('products as a').where('a.status', 1)
            .join('product_images as b', 'a.id', 'b.product_id').select('a.*', 'b.image')
        // console.log(res);

        return res
    }
    public async productOtherPromotion(ctx: HttpContextContract) {
        const data = ctx.request.all()
        console.log(data);
        
        const res = await Database.from('promotion_products as a').where('a.product_id', data.id)
            .join('product_others as b', 'a.product_id', 'b.product_id')
        console.log(res);
        const tableImage = await Database.from('product_other_images')
        res.map(edit => {
            for (let i = 0; i < tableImage.length; i++) {
                if (edit.id == tableImage[i].product_other_id)
                    edit.image = tableImage[i].image
            }
        })
        return res
    }

    public async promotionProductOther(ctx: HttpContextContract) {
        const data = ctx.request.all()
        // console.log(data);

        // console.log(JSON.stringify(data.product_other));
        const covertObjToString = JSON.stringify(data.product_other)
        const res = await PromotionProduct.query().where('product_id', data.product_id).update({
            product_other: covertObjToString
        })
        if (res) {
            return { success: true, message: 'บันทึกข้อมูลโปรโมชั่นเรียนร้อย' }
        } else {
            return { success: false, message: 'บันทึนไม่ผ่าน!!!' }
        }
    }
    public async showPromotionProductSingle(ctx: HttpContextContract) {
        const res = await Database.from('promotion_products').where('product_id', ctx.request.all().product_id).first()
        return res

    }
}
