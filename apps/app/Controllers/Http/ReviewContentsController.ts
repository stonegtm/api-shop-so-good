import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
import ReviewContent from "App/Models/ReviewContent";
import ReviewCustomer from "App/Models/ReviewCustomer";

const fs = require("fs");
("use strict");
import Database from "@ioc:Adonis/Lucid/Database";
export default class ReviewContentsController {
    public async imageReviewContent(ctx: HttpContextContract) {
        //เก็บพารามมาจากหน้า
        const { name } = ctx.request.params();
        //เรียกรูปภาพออกไปโชว์
        const getStream = await Drive.getStream(
            Application.tmpPath("uploads/review/" + name)
        );
        ctx.response.stream(getStream);
    }
    public async getReview(ctx: HttpContextContract) {
        const res = await ReviewContent.query()
        return res
    }
    public async getSigleReview(ctx: HttpContextContract) {
        const { id } = ctx.request.params();
        const res = await ReviewContent.query().where('id', id).first()
        return res
    }
    public async editReview(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const image = ctx.request.file('image')
        const regex = /\s/g;

        const d = new Date();
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        const newname = `${addZero(d.getSeconds())}${addZero(
            d.getMinutes()
        )}${addZero(d.getHours())}${d.getDate()}${d.getMonth() + 1
            }${d.getFullYear()}`;

        if (image) {
            const tableReview = await Database.from('review_contents').where('id', data.id).first()
            // console.log(tableReview.image);
            const newName = newname.toString() + image.data.clientName;

            if (tableReview) {
                await fs.unlink(
                    Application.tmpPath("uploads/review/") + tableReview.image,
                    async (err) => {
                        if (err) {
                            console.log("failed to delete local image:" + err);
                        }
                    }
                );

                await image.move(Application.tmpPath("uploads/review/"), {
                    name: newName,
                    overwrite: true,
                });
                const res = await ReviewContent.query().where('id', data.id).update({
                    title: data.title,
                    title_code: data.title.replace(regex, "_"),
                    description_short: data.description_short,
                    description_full: data.description_full,
                    image: newName,
                });
                if (res) {
                    return { success: true, message: 'สร้างสำเร็จแล้ว' }
                }
            }

        } else {
            const res = await ReviewContent.query().where('id', data.id).update({
                title: data.title,
                title_code: data.title.replace(regex, "_"),
                description_short: data.description_short,
                description_full: data.description_full,
            });
            if (res) {
                return { success: true, message: 'แก้ไขสำเร็จแล้ว' }
            }
        }
    }
    public async index(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const image = ctx.request.file('image')
        const regex = /\s/g;

        const d = new Date();
        function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
        const newname = `${addZero(d.getSeconds())}${addZero(
            d.getMinutes()
        )}${addZero(d.getHours())}${d.getDate()}${d.getMonth() + 1
            }${d.getFullYear()}`;
        const newName = newname.toString() + image.data.clientName;

        if (image) {
            await image.move(Application.tmpPath("uploads/review/"), {
                name: newName,
                overwrite: true,
            });
            const res = await ReviewContent.create({
                title: data.title,
                title_code: data.title.replace(regex, "_"),
                description_short: data.description_short,
                description_full: data.description_full,
                image: newName,
            });
            if (res) {
                return { success: true, message: 'สร้างสำเร็จแล้ว' }
            }
        }
    }
    public async getAllreview(ctx: HttpContextContract) {
        const res = await ReviewContent.query().orderBy('created_at', 'desc').limit(6)
        return res
    }
    public async getSingleReview(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await ReviewContent.query().where('title_code', data.title_code).first()
        return res
    }
    public async deleteReview(ctx: HttpContextContract) {
        const data = ctx.request.all()
        // console.log(data);
        const res = await ReviewContent.query().where('id', data.id).delete()
        if (res) {
            await fs.unlink(
                Application.tmpPath("uploads/review/") + data.image,
                async (err) => {
                    if (err) {
                        console.log("failed to delete local image:" + err);
                    }
                }
            );
            return { success: true, message: 'ลบข้อมูลเรียบร้อยแล้ว' }
        }
    }

    public async pickProductForReview(ctx: HttpContextContract) {
        const data = ctx.request.all()
        // console.log(data);
        
        const res = await Database.from('products as a').where('a.id', data.product_id).where("a.status", 1)
            .leftJoin('brands as z', 'a.brand_id', 'z.id')
            .join("product_details as c", "a.id", "c.product_id")
            .join("product_images as d", "a.id", "d.product_id")
            .join("product_new_arrivals as e", "a.id", "e.product_id")
            .leftJoin('promotion_products as g', 'a.id', 'g.product_id')
            .select("a.*", "c.description", "d.*", 'e.*', "g.discount as discount_promotion", 'z.name as name_brands').first()
        if (res) {
            return { success: true, data: res }
        } else {
            return { success: false }
        }
    }
    public async getNameUser(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await Database.from('customer_profiles').where('customer_id', data.id).first()
        // console.log(res);
        if (res) {
            return { success: true, data: res.fullname }
        } else {
            return { success: false }
        }
    }
    public async createReviewCustomer(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const image = ctx.request.file('image')
        const time = new Date().getTime();
        const newName = 'review_' + time + '.jpg'
        const regex = /\s/g;
        if (image) {
            await image.move(Application.tmpPath("uploads/review/"), {
                name: newName,
                overwrite: true,
            });
            const res = await ReviewCustomer.create({
                title: data.title,
                code_name: data.title.replace(regex, "_"),
                title_sub: data.title_sub,
                description: data.description,
                product_id: data.product_id,
                customer_id: data.customer_id,
                rating: data.rating,
                status: true,
                image: newName,
            });
            if (res) {
                return { success: true, message: 'เขียนรีวิวสำเร็จแล้ว' }
            } else {
                return { success: false, message: 'เขียนรีวิวไม่สำเร็จแล้ว' }
            }
        }
    }
    public async showAllReviewCustomer(ctx: HttpContextContract) {
        const res = await Database.from('review_customers')
        return res
    }
    public async showSinglelReviewCustomer(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await Database.from('review_customers as a').where('a.code_name', data.code_name)
            .join('products as b', 'a.product_id', 'b.id')
            .leftJoin('brands as f', 'b.brand_id', 'f.id')
            .leftJoin('customer_profiles as e', 'a.customer_id', 'e.customer_id')
            .leftJoin('product_images as c', 'a.product_id', 'c.product_id').where('c.main', 1)
            .leftJoin('product_details as d', 'a.product_id', 'd.product_id')
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select("a.*","a.image as image_review","a.description as description_review","f.name as brands_name", "d.description", "b.*", 'c.*', "g.discount as discount_promotion", 'e.*').first()
        // console.log(res);
        return res
    }
}
