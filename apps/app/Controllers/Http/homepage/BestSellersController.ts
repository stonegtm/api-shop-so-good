import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
import ProductBestseller from "App/Models/ProductBestseller";

("use strict");
import Application from "@ioc:Adonis/Core/Application";
export default class BestSellersController {
    public async bestSellersGet(ctx: HttpContextContract) {
        const res = await Database.from('product_solds as a')
            .orderBy('quantity', 'desc')
            .join('products as b', 'b.id', 'a.product_id')
            .join('product_images as c', 'c.product_id', 'a.product_id')
            .where('main', 1)
        return res
    }
    public async addBestSellers(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const checkSame = await Database.from('product_bestsellers').where('product_id', data.product_id).first()
        if (!checkSame) {
            const haved = await Database.from("product_bestsellers")

            if (haved.length >= 1) {
                const res = await ProductBestseller.create({ product_id: data.product_id, rating: haved.length + 1 })
                if (res) {
                    return { success: true, message: 'เพิ่มสำเร็จแล้ว' }
                }
            }
            else {
                const res = await ProductBestseller.create({ product_id: data.product_id, rating: 1 })
                if (res) {
                    return { success: true, message: 'เพิ่มสำเร็จแล้ว' }
                }
            }
        } else {
            return { success: false, message: 'มีสินค้าชิ้นนี้แล้ว!!!' }
        }

    }
    public async bestSellersPicked(ctx: HttpContextContract) {
        const res = await Database.from('product_bestsellers as a').join('products as b', 'a.product_id', 'b.id').join('product_images as c', 'a.product_id', 'c.product_id').where('c.main', 1)
        return { success: true, data: res }
    }
    public async bestSellersPickedFront(ctx: HttpContextContract) {
        const res = await Database.from('product_bestsellers as a')
            .join('products as b', 'a.product_id', 'b.id')
            .where("b.status", 1)
            .join("product_details as c", "a.product_id", "c.product_id")
            .join("product_images as d", "a.product_id", "d.product_id")
            .join("product_new_arrivals as e", "a.product_id", "e.product_id")
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select("a.rating", "b.*", "c.description", "d.*", "e.*", "g.discount as discount_promotion").orderBy('a.rating')
        const pdo = await Database.from('product_others')

        return { success: true, data: res, product_other: pdo }
    }
    public async bestSellersAllPickedFront(ctx: HttpContextContract) {
        const tableProduct = await Database.from('product_bestsellers as a')
            .join('products as aa', 'a.product_id', 'aa.id').where('aa.status', 1)
            .join('product_images as b', 'a.product_id', 'b.product_id').where('b.main', 1)
            .join('product_details as c', 'a.product_id', 'c.product_id')
            .join('product_new_arrivals as d', 'a.product_id', 'd.product_id')
            .join('product_seos as e', 'a.product_id', 'e.product_id')
            .join('stocks as f', 'a.product_id', 'f.product_id')
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select('a.*', 'aa.*', 'b.*', 'c.*', 'd.*', 'e.*', 'f.*', 'g.discount as discount_promotion',).orderBy('a.rating')
        const tableProductOther = await Database.from('product_others')
        tableProduct.map(edit => {
            edit.pdo = []
            for (let i = 0; i < tableProductOther.length; i++) {
                if (edit.id == tableProductOther[i].product_id) {
                    edit.pdo.push(tableProductOther[i])
                }
            }
        })
        if (tableProduct.length >= 1) {
            return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
        } else {
            return { success: false, message: 'ดึงข้อมูลได้!!!' }
        }
    }

    public async deletePickedBestSellers(ctx: HttpContextContract) {
        // const res = await Database.from('product_bestsellers as a').join('products as b', 'a.product_id', 'b.id').join('product_images as c', 'a.product_id', 'c.product_id').where('c.main', 1)
        // return { success: true, data: res }
        const data = ctx.request.all()
        const res = await ProductBestseller.query().where('product_id', data.product_id).delete()
        if (res) {
            return { success: true, message: 'ลบสำเร็จแล้ว' }
        } else {
            return { success: false, message: 'ลบไม่สำเร็จแล้ว' }
        }
    }
    public async nameProductForAdd(ctx: HttpContextContract) {
        const res = await Database.from('products as a').where('a.status', 1)
        .join('product_images as b', 'a.id', 'b.product_id').select('a.*','b.image')
        return res
    }



}
