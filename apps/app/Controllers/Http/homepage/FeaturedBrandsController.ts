import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
import Database from "@ioc:Adonis/Lucid/Database";
import FeaturedBrand from "App/Models/FeaturedBrand";

("use strict");
export default class FeaturedBrandsController {
  public async imageFeatureForShow(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/featured-brand/" + name)
    );
    ctx.response.stream(getStream);
  }
  public async createFeatureBrand(ctx: HttpContextContract) {
    const image1 = ctx.request.file("image1");
    const image2 = ctx.request.file("image2");
    const { id } = ctx.auth.user?.toJSON();
    const data = ctx.request.all();
    const checkEvenNumber = await Database.from("featured_brands").select();

    const d = new Date();
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    const order_number = `${addZero(d.getSeconds())}${addZero(
      d.getMinutes()
    )}${addZero(d.getHours())}${d.getDate()}${d.getMonth() + 1
      }${d.getFullYear()}`;
    const newName1 = order_number.toString() + image1.data.clientName;
    const newName2 = order_number.toString() + image2.data.clientName;

    if (image1) {
      await image1.move(Application.tmpPath("uploads/featured-brand"), {
        name: newName1,
        overwrite: true,
      });
      if (image2) {
        await image2.move(Application.tmpPath("uploads/featured-brand"), {
          name: newName2,
          overwrite: true,
        });
        if (checkEvenNumber.length % 2 == 0) {
          const res = await FeaturedBrand.create({
            rating: data.rating,
            status: 1,
            is_delete: 0,
            image_1: newName1,
            image_2: newName2,
            index1: 4,
            index2: 3,
            alt: data.alt,
          });
          if (res) {
            return {
              success: true,
              message: "อัพโหลด Feature Brand สำเร็จเรียบร้อย",
            };
          } else {
            return {
              success: false,
              message: "อัพโหลด Feature Brand ไม่สำเร็จ",
            };
          }
        } else {
          const res = await FeaturedBrand.create({
            rating: data.rating,
            status: 1,
            is_delete: 0,
            image_1: newName1,
            image_2: newName2,
            index1: 2,
            index2: 1,
            alt: data.alt,
          });
          if (res) {
            return {
              success: true,
              message: "อัพโหลด Feature Brand สำเร็จเรียบร้อย",
            };
          } else {
            return {
              success: false,
              message: "อัพโหลด Feature Brand ไม่สำเร็จ",
            };
          }
        }
      }
    }
  }
  public async getFeature(ctx: HttpContextContract) {
    const res = await FeaturedBrand.query().where("is_delete", 0)
    return res;
  }
  public async getFeatureFront(ctx: HttpContextContract) {
    const res = await FeaturedBrand.query().where('status', 1);
    return res;
  }
  public async editAndDeleteFeatureBrand(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await FeaturedBrand.query().where("id", data.id).update({
      status: data.status,
      is_delete: data.is_delete,
    });
    if (res) {
      return {
        success: true,
        message: "แก้ไขเรียบร้อย",
      };
    } else {
      return {
        success: false,
        message: "แก้ไขเรียบร้อยไม่สำเร็จ",
      };
    }
  }
}
