import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import ProductNewArrival from "App/Models/ProductNewArrival";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
("use strict");
import Application from "@ioc:Adonis/Core/Application";
export default class NewarrivalsController {
  public async editNewArrival(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await ProductNewArrival.query()
      .where("product_id", data.product_id)
      .update({
        newarrival_status: 0,
      });
    if (res) {
      return { success: true, message: "คุณทำการลบสำเร็จแล้ว" };
    }
  }
  public async editRatingNewArrival(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await ProductNewArrival.query()
      .where("product_id", data.product_id)
      .update({
        rating: data.rating,
      });
    if (res) {
      return { success: true, message: "เปลี่ยนลับดับสำเร็จแล้ว" };
    }
  }
}
