import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import CarouselHomepage from "App/Models/homepage/CarouselHomepage";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
("use strict");
import Application from "@ioc:Adonis/Core/Application";

export default class CarouselsController {
  public async uploadImageCarousel(data, rating, alt) {
    const addBeforeNameImg = new Date().getTime();
    const newNameImg = addBeforeNameImg + data.data.clientName;
    await data.move(Application.tmpPath("uploads/homepage/carousel"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableCarousel = new CarouselHomepage();
    tableCarousel.carousel_image = newNameImg;
    tableCarousel.rating = rating;
    tableCarousel.carousel_alt = alt;
    await tableCarousel.save();
    return { success: true, message: newNameImg };
  }
  public async index(ctx: HttpContextContract) {
    const data = await ctx.request.all();
    const coverImage = ctx.request.files("image");
    // console.log(coverImage);
    if (coverImage) {
      coverImage.map(async (dataImage) => {
        const res = await this.uploadImageCarousel(
          dataImage,
          data.rating,
          data.carousel_alt
        );
      });
      return { success: true, message: "เพิ่มรูปสำเร็จ" };
    }
    return true;
  }

  public async selectCarouselImage(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/homepage/carousel/" + name)
    );
    ctx.response.stream(getStream);
  }

  public async selectCarouselNameImage(ctx: HttpContextContract) {
    const data = await CarouselHomepage.query().where("status", 1).orderBy("rating", "asc");
    return data;
  }
  public async selectCarouselBackend(ctx: HttpContextContract) {
    const data = await CarouselHomepage.query().orderBy("rating", "asc");
    return data;
  }

  public async editDataCarousel(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await CarouselHomepage.query().where("id", data.id).update({
      rating: data.rating,
      carousel_alt: data.carousel_alt,
      status: data.status,
    });
    if (res) {
      return { success: true, message: "แก้ไขสำเร็จ" };
    } else {
      return { success: false, message: "แก้ไขไม่สำเร็จ" };
    }
  }

  public async deleteCarousel(ctx: HttpContextContract) {
    const data = ctx.request.all();
    await fs.unlink(
      Application.tmpPath("uploads/homepage/carousel/") + data.carousel_image,
      async (err) => {
        if (err) {
          console.log("failed to delete local image:" + err);
        } else {
          const res = await CarouselHomepage.query()
            .where("id", data.id)
            .delete();
        }
      }
    );
    return { success: true, message: "ลบสำเร็จ" };
  }
}
