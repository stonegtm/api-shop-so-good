import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
("use strict");
import Database from "@ioc:Adonis/Lucid/Database";

export default class CategoryAndBrandsController {
    public async getByCategoryCodeName(ctx: HttpContextContract) {
        const { code_name } = ctx.request.params()
        if (code_name == 'all-product') {
            const tableProduct = await Database.from('products as a').where('a.status', 1)
                .join('product_images as b', 'a.id', 'b.product_id').where('b.main', 1)
                .join('product_details as c', 'a.id', 'c.product_id')
                .join('product_new_arrivals as d', 'a.id', 'd.product_id')
                .join('product_seos as e', 'a.id', 'e.product_id')
                .join('stocks as f', 'a.id', 'f.product_id')
                .leftJoin('promotion_products as g', 'a.id', 'g.product_id')
                .select("a.*", "b.*", "c.*", 'd.*', "e.*", "f.*", "g.discount as discount_promotion")
            const tableProductOther = await Database.from('product_others')
            tableProduct.map(edit => {
                edit.pdo = []
                for (let i = 0; i < tableProductOther.length; i++) {
                    if (edit.id == tableProductOther[i].product_id) {
                        edit.pdo.push(tableProductOther[i])
                    }
                }
            })
            if (tableProduct.length >= 1) {
                return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
            } else {
                return { success: false, message: 'ดึงข้อมูลได้!!!' }
            }
        } else {
            const tableCategory = await Database.from('category_mains as a').where('a.code_name', code_name).first()
            // console.log(tableCategory);

            const tableProduct = await Database.from('products as a').where('a.status', 1)
                .join('product_images as b', 'a.id', 'b.product_id').where('b.main', 1)
                .join('product_details as c', 'a.id', 'c.product_id')
                .join('product_new_arrivals as d', 'a.id', 'd.product_id')
                .join('product_seos as e', 'a.id', 'e.product_id')
                .join('stocks as f', 'a.id', 'f.product_id')
            const tableProductOther = await Database.from('product_others')
            tableProduct.map(edit => {
                edit.pdo = []
                for (let i = 0; i < tableProductOther.length; i++) {
                    if (edit.id == tableProductOther[i].product_id) {
                        edit.pdo.push(tableProductOther[i])
                    }
                }
            })
            const dataProduct = []
            for (let i = 0; i < tableProduct.length; i++) {
                let category = JSON.parse(tableProduct[i].category);
                // console.log(category);
                for (let b = 0; b < category.length; b++) {
                    if (tableCategory.id == category[b].id) {
                        dataProduct.push(tableProduct[i])
                    }
                }

            }
            console.log(dataProduct);

            if (dataProduct.length >= 1) {
                return { success: true, data: dataProduct, message: 'ดึงข้อมูลได้!!!' }
            } else {
                return { success: false, message: 'ดึงข้อมูลได้!!!' }
            }
        }
    }
    public async getByBrandCodeName(ctx: HttpContextContract) {
        const { code_name } = ctx.request.params()
        // console.log(code_name);

        if (code_name == 'all-product') {
            const tableProduct = await Database.from('products as a').where('a.status', 1)
                .join('product_images as b', 'a.id', 'b.product_id').where('b.main', 1)
                .join('product_details as c', 'a.id', 'c.product_id')
                .join('product_new_arrivals as d', 'a.id', 'd.product_id')
                .join('product_seos as e', 'a.id', 'e.product_id')
                .join('stocks as f', 'a.id', 'f.product_id')
                .leftJoin('promotion_products as g', 'a.id', 'g.product_id')
                .select("a.*", "b.*", "c.*", 'd.*', "e.*", "f.*", "g.discount as discount_promotion")
            const tableProductOther = await Database.from('product_others')
            tableProduct.map(edit => {
                edit.pdo = []
                for (let i = 0; i < tableProductOther.length; i++) {
                    if (edit.id == tableProductOther[i].product_id) {
                        edit.pdo.push(tableProductOther[i])
                    }
                }
            })
            if (tableProduct.length >= 1) {
                return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
            } else {
                return { success: false, message: 'ดึงข้อมูลได้!!!' }
            }
        } else {
            const tableCategory = await Database.from('brands as a').where('a.code_name', code_name).first()
            // console.log(tableCategory);

            const tableProduct = await Database.from('products as a').where('a.status', 1).where('a.brand_id', tableCategory.id)
                .join('product_images as b', 'a.id', 'b.product_id').where('b.main', 1)
                .join('product_details as c', 'a.id', 'c.product_id')
                .join('product_new_arrivals as d', 'a.id', 'd.product_id')
                .join('product_seos as e', 'a.id', 'e.product_id')
                .join('stocks as f', 'a.id', 'f.product_id')

            const tableProductOther = await Database.from('product_others')
            tableProduct.map(edit => {
                edit.pdo = []
                for (let i = 0; i < tableProductOther.length; i++) {
                    if (edit.id == tableProductOther[i].product_id) {
                        edit.pdo.push(tableProductOther[i])
                    }
                }
            })
            // console.log(tableProduct);

            // const dataProduct = []
            // for (let i = 0; i < tableProduct.length; i++) {
            //     let category = JSON.parse(tableProduct[i].category);
            //     if (tableCategory.id == category[0].id) {
            //         dataProduct.push(tableProduct[i])
            //     }
            // }

            if (tableProduct.length >= 1) {
                return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
            } else {
                return { success: false, message: 'ดึงข้อมูลได้!!!' }
            }
        }
    }

}
