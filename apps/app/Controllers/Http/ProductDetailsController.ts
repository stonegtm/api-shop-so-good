import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
("use strict");
import ProductDetail from "App/Models/Brand";
export default class ProductDetailsController {
  public async index(ctx: HttpContextContract) {
    const data = ctx.request.all();
    if (data) {
      await ProductDetail.create(data);
      return { success: true, message: "สร้าง Category สำเร็จ" };
    } else {
      return { success: false, message: "สร้าง Category ไม่สำเร็จ" };
    }
  }
}
