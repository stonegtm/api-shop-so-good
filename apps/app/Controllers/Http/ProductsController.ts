import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";

("use strict");
import Product from "App/Models/Product";
import ProductImage from "App/Models/ProductImage";
import ProductDescriptionImage from "App/Models/ProductDescriptionImage";
import ProductDetail from "App/Models/ProductDetail";
import ProductColor from "App/Models/ProductColor";
import ProductOther from "App/Models/ProductOther";
import ProductSize from "App/Models/ProductSize";
import Stock from "App/Models/Stock";
import ProductSeo from "App/Models/ProductSeo";
import ProductNewArrival from "App/Models/ProductNewArrival";
import ProductOtherImage from "App/Models/ProductOtherImage";

export default class ProductsController {
  public async productLog(data, id) {
    const arr_log = [
      {
        product_id: id,
        message: data
      }
    ]
    console.log(arr_log);


  }
  public async imageDescriptionForUpload(data, id) {
    //เพิ่มวันที่ต่อหน้าชื่อ

    const addBeforeNameImg = new Date().getTime();
    const regex = /\s/g;

    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + data.data.clientName.replace(regex, '_');
    // อัพรูปลงเครื่อง api
    await data.move(Application.tmpPath("uploads/product/description"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableImage = new ProductDescriptionImage();
    tableImage.image = newNameImg;
    tableImage.product_id = id;
    await tableImage.save();

    return { success: true, message: newNameImg };
  }
  public async imageForUpload(data, id, isMain) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const regex = /\s/g;

    const addBeforeNameImg = new Date().getTime();

    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + data.data.clientName.replace(regex, '_');
    // อัพรูปลงเครื่อง api
    await data.move(Application.tmpPath("uploads/product"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableImage = new ProductImage();
    tableImage.image = newNameImg;
    tableImage.product_id = id;
    tableImage.main = isMain;
    await tableImage.save();

    return { success: true, message: newNameImg };
  }
  public async uploadImageEdit(data, product_id, main) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const regex = /\s/g;

    const addBeforeNameImg = new Date().getTime();
    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + data.data.clientName.replace(regex, '_');
    // อัพรูปลงเครื่อง api
    await data.move(Application.tmpPath("uploads/product"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableImage = new ProductImage();
    tableImage.image = newNameImg;
    tableImage.product_id = product_id;
    tableImage.main = main;
    // tableImage.main = isMain;
    await tableImage.save();

    return { success: true, message: newNameImg };
  }

  public async uploadImageDetailEdit(data, product_id) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const addBeforeNameImg = new Date().getTime();
    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const regex = /\s/g;

    const newNameImg = addBeforeNameImg + data.data.clientName.replace(regex, '_');
    // อัพรูปลงเครื่อง api
    await data.move(Application.tmpPath("uploads/product/description"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableImage = new ProductDescriptionImage();
    tableImage.image = newNameImg;
    tableImage.product_id = product_id;

    // tableImage.main = isMain;
    await tableImage.save();

    return { success: true, message: newNameImg };
  }

  //โชว์รูปของ Product
  public async selectImageBrands(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/brands/" + name)
    );
    ctx.response.stream(getStream);
  }
  //โชว์รูปของ Product Description Image
  public async selectImageProductDescription(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/product/description/" + name)
    );
    ctx.response.stream(getStream);
  }

  public async index(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // const { id } = ctx.auth.user?.toJSON();


    //  const reName =  data.name.replaceAll(' ', '_')
    const regex = /\s/g;
    // console.log(data.name.replace(regex, "_"));

    const tableProduct = new Product();
    tableProduct.name = data.name;
    tableProduct.code_name = data.name.replace(regex, "_");
    tableProduct.category = data.category;
    tableProduct.status = data.status;
    tableProduct.brand_id = data.brand_id;
    tableProduct.price = data.price;
    tableProduct.discount = data.discount || 0;
    tableProduct.is_delete = data.is_delete || 0;
    const res = await tableProduct.save();
    const tableProductColor = new ProductColor();
    tableProductColor.product_id = res.id;
    tableProductColor.color = data.color;
    tableProductColor.code_color = data.color.replace(regex, "_");
    await tableProductColor.save();
    const tableProductSize = new ProductSize();
    tableProductSize.product_id = res.id;
    tableProductSize.size = data.size;
    tableProductSize.code_size = data.size.replace(regex, "_");
    await tableProductSize.save();
    tableProductColor.product_id = res.id;
    const tableProductDetail = new ProductDetail();
    tableProductDetail.description = data.description || null;
    tableProductDetail.product_id = res.id;
    await tableProductDetail.save();
    const tableStock = new Stock();
    tableStock.quantity = data.quantity || 0;
    tableStock.product_id = res.id;
    await tableStock.save();
    const tableProductSeo = new ProductSeo();
    tableProductSeo.meta_title = data.meta_title;
    tableProductSeo.meta_description = data.meta_description;
    tableProductSeo.meta_keywords = data.meta_keywords;
    tableProductSeo.product_id = res.id;
    await tableProductSeo.save();
    const tableProductNewArrival = new ProductNewArrival();
    tableProductNewArrival.newarrival_status = data.newarrival_status;
    tableProductNewArrival.product_id = res.id;
    await tableProductNewArrival.save();
    //เพิ่ม function product hitory 
    //บอกข้อความผลลัพธ์ add new product
    const coverImage = ctx.request.files("image");
    const image_description = ctx.request.files("image_description");
    if (image_description) {
      image_description.map(async (dataImage, index) => {
        await this.imageDescriptionForUpload(dataImage, res.id);
      });
    }
    if (coverImage) {
      coverImage.map(async (dataImage, index) => {
        await this.imageForUpload(dataImage, res.id, index == data.main);
      });
    }
    return { success: true, message: "สร้าง Product สำเร็จ" };
  }
  //image product other show

  public async showProductOther(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/product/other/" + name)
    );
    ctx.response.stream(getStream);
  }
  public async imageProductOther(ctx: HttpContextContract) {
    const data = ctx.request.all()
    const res = await Database.from('product_other_images').where('product_other_id', data.id)
    if (res) {
      return { success: true, data: res }
    } else {
      return { success: false }

    }

  }

  public async uploadImageProductOther(data, product_other_id,) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const addBeforeNameImg = new Date().getTime();
    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const regex = /\s/g;

    const newNameImg = addBeforeNameImg + data.data.clientName.replace(regex, '_');
    // อัพรูปลงเครื่อง api
    await data.move(Application.tmpPath("uploads/product/other"), {
      name: newNameImg,
      overwrite: true,
    });
    const tableOtherImage = new ProductOtherImage();
    tableOtherImage.image = newNameImg;
    tableOtherImage.product_other_id = product_other_id;
    await tableOtherImage.save();
    return { success: true };
  }
  //add product other
  public async addProductOhter(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data);
    const regex = /\s/g;
    const tableProductOther = new ProductOther();
    tableProductOther.product_id = data.product_id;
    tableProductOther.price = data.price;
    tableProductOther.discount = data.discount;
    tableProductOther.color = data.color;
    tableProductOther.code_color = data.code_color || null;
    tableProductOther.size = data.size;
    tableProductOther.code_size = data.size.replace(regex, "_") || null;
    tableProductOther.quantity = data.quantity;
    const res = await tableProductOther.save();

    const coverImage = ctx.request.files("image");
    if (coverImage) {
      coverImage.map(async (dataImage, index) => {
        await this.uploadImageProductOther(dataImage, res.id);
      });
      return { success: true, message: 'เพิ่มสินค้าสำเร็จ' }
      // const res = await ProductOther.create({
      //   product_id: data.product_id,
      //   price: data.price,
      //   discount: data.discount || 0,
      //   color: data.color || false,
      //   code_color: data.color.replace(regex, "_") || null,
      //   size: data.size || false,
      //   code_size: data.size.replace(regex, "_") || null,
      //   quantity: data.quantity
      // })
    }
  }

  public async getOther(ctx: HttpContextContract) {
    const { id } = ctx.request.params();
    const res = await ProductOther.query().where('product_id', id)
    return res
  }


  //GET PRODUCT BY NEW ARRIVAL
  public async getProductNewArrival(ctx: HttpContextContract) {
    const res = await Database.from("product_new_arrivals as a")
      .where("newarrival_status", 1)
      .join("products as b", "a.product_id", "b.id")
      .where("b.status", 1)
      .join("product_details as c", "a.product_id", "c.product_id")
      .join("product_images as d", "a.product_id", "d.product_id")
      .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
      .select("a.newarrival_status", "a.rating", "b.*", "c.description", "d.*", "g.discount as discount_promotion")
      .where("d.main", 1)
      .orderBy("rating", "asc");


    const pdo = await Database.from('product_others')


    return { product: res, product_other: pdo };
  }

  //GET BY ID
  public async getProduct(ctx: HttpContextContract) {
    const { id } = ctx.request.params();
    let tableProduct = await Product.query().where("id", id).first();
    const detail = await tableProduct
      ?.related("productdetails")
      .query()
      .first();
    const image = await tableProduct?.related("productimage").query();
    const imageDetail = await tableProduct?.related("productDescriptionImage").query();
    const seo = await tableProduct?.related("productseo").query().first();
    const stock = await tableProduct?.related("productstock").query().first();
    const newarrival = await tableProduct
      ?.related("productnewarrival")
      .query()
      .first();

    const data = { tableProduct, detail, image, seo, stock, newarrival, imageDetail };

    return data;
    // console.log(id);
  }
  //get test backend 
  public async getProductTest(ctx: HttpContextContract) {
    const { id } = ctx.request.all();
    const res = await Product.query().where("id", id).first();
    const image = await res?.related("productimage").query();
    const stock = await res?.related("productstock").query().first();
    const product_other = await res?.related("productOther").query();
    const detail = await res?.related("productdetails").query().first();
    const seo = await res?.related("productseo").query().first();


    // console.log(res?.toJSON());
    const name_brand = await Database.from("brands")
      .where("id", res?.brand_id)
      .select("name")
      .first();

    // console.log(name_brand);
    const passCategory = JSON.parse(res?.category);
    // console.log(passCategory[0].id);
    const tableCategoryMain = await Database.from("category_mains")
      .where("id", passCategory[0].id)
      .first();
    // console.log(tableCategoryMain);
    const category = {
      nameweb: "seoulgood",
      nameCategory: tableCategoryMain.name,
    };

    if (product_other) {
      let sum = 0
      for (let i = 0; i < product_other.length; i++) {
        sum += product_other[i].quantity
      }
      const stockAll = stock?.quantity + sum
      const data = { res, stockAll, image, detail, name_brand, category, product_other, seo };
      return data;

    } else {
      const data = { res, stock, image, detail, name_brand, category, seo };
      return data;
    }
  }
  //get product color single
  public async getProductColor(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data);
    if (data.twin) {
      const res = await Database.from('product_others').where('product_id', data.product_id).where('color', data.color).where('size', data.size).first()
      console.log(res);
      if (res) {
        return { success: true, quantityColor: res.quantity, price: res.price, discount: res.discount }
      } else {
        return { success: true, quantityColor: 0, price: 0 }
      }
    }
    else if (data.color) {
      const res = await Database.from('product_others').where('product_id', data.product_id).where('color', data.color)
      let sumQuantity = 0
      let price = 0
      let discount = 0
      // console.log(res.length);

      if (res.length > 1) {
        for (let i = 0; i < res.length; i++) {
          sumQuantity += res[i].quantity
          price = res[i].price
          discount = res[i].discount
        }
        return { success: true, quantityColor: sumQuantity, price: price, discount: discount }
      }
      else {
        sumQuantity += res[0].quantity
        price = res[0].price
        discount = res[0].discount
        return { success: true, quantityColor: sumQuantity, price: price, discount: discount }
      }
    } else if (data.size) {
      const res = await Database.from('product_others').where('product_id', data.product_id).where('size', data.size)
      let sumQuantity = 0
      let price = 0
      let discount = 0
      if (res.length > 1) {
        for (let i = 0; i < res.length; i++) {
          sumQuantity += res[i].quantity
          price = res[i].price
          discount = res[i].discount
        }
        return { success: true, quantityColor: sumQuantity, price: price, discount: discount }
      }
      // for (let i = 0; i < res.length; i++) {
      //   sumQuantity += res[i].quantity
      // }
      else {
        sumQuantity += res[0].quantity
        price = res[0].price
        discount = res[0].discount
        return { success: true, quantityColor: sumQuantity, price: price, discount: discount }
      }
    }
    // let sumQuantity = 0
    // for (let i = 0; i < res.length; i++) {
    //   sumQuantity += res[i].quantity
    // }
    // console.log(sumQuantity);
    // console.log(res);

    // return { success: true, quantityColor: sumQuantity }

  }

  //GET SINGLE ROUNTE POST
  public async getProductShowSingle(ctx: HttpContextContract) {
    const { id } = ctx.request.all();
    const { customer_id } = ctx.request.all();
    const res = await Product.query().where("code_name", id).first();
    const image = await res?.related("productimage").query();
    const product_other = await res?.related("productOther").query();
    const product_description_image = await res?.related('productDescriptionImage').query()
    const detail = await res?.related("productdetails").query().first();
    const seo = await res?.related("productseo").query().first();
    const promotion_product = await res?.related("promotionproduct").query().first();

    // const data001 = await Database.from('products as a').where('a.code_name', id)

    if (promotion_product) {
      const parse = JSON.parse(promotion_product.product_other)
      console.log(parse);

      if (parse.length >= 1) {
        product_other?.map(edit => {
          for (let i = 0; i < parse.length; i++) {
            if (edit.id == parse[i].product_other_id) {
              edit.discount_promotion = parseInt(parse[i].discount_other_promotion)
            }
          }
        });
      }
    }
    const name_brand = await Database.from("brands")
      .where("id", res?.brand_id)
      .first();

    // console.log(name_brand);
    const passCategory = JSON.parse(res?.category);
    // console.log(passCategory[0].id);
    const tableCategoryMain = await Database.from("category_mains")
      .where("id", passCategory[0].id)
      .first();
    // console.log(tableCategoryMain);
    const category = {
      nameweb: "seoulgood",
      nameCategory: tableCategoryMain.name,
      code_name: tableCategoryMain.code_name,
    };
    if (customer_id) {
      const favorite = await Database.from('customer_favorite_products')
        .where('customer_id', customer_id).where('product_id', res.id).first()
      // console.log(favorite);
      if (product_other) {
        if (promotion_product) {
          const data = { res, favorite, product_description_image, image, detail, name_brand, category, product_other, seo, promotion_product };
          return data;
        } else {
          const data = { res, favorite, product_description_image, image, detail, name_brand, category, product_other, seo };
          return data;
        }
      } else {
        if (promotion_product) {
          const data = { res, favorite, product_description_image, image, detail, name_brand, category, seo, promotion_product };
          return data;

        } else {
          const data = { res, favorite, product_description_image, image, detail, name_brand, category, seo };
          return data;

        }
      }
    } else {
      if (product_other) {
        if (promotion_product) {
          const data = { res, product_description_image, image, detail, name_brand, category, product_other, seo, promotion_product };
          return data;
        } else {
          const data = { res, product_description_image, image, detail, name_brand, category, product_other, seo };
          return data;
        }
      } else {
        if (promotion_product) {
          const data = { res, product_description_image, image, detail, name_brand, category, seo, promotion_product };
          return data;
        } else {
          const data = { res, product_description_image, image, detail, name_brand, category, seo };
          return data;
        }

      }
    }

  }

  public async getProductMain(ctx: HttpContextContract) {
    const res = await Database.from("products as a")
      .where("is_delete", "!=", 1)
      .join("brands as b", "a.brand_id", "b.id")
      .join("stocks as c", "a.id", "c.product_id")
      .select("a.*", "b.name as brand", "c.quantity").orderBy('id')
    const pdo = await Database.from('product_others')
    // console.log(pdo);


    return { data: res, pdo: pdo };
  }

  public async getImageProduct(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    // console.log(name);

    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/product/" + name)
    );
    ctx.response.stream(getStream);
  }

  public async getNameImageByProductId(ctx: HttpContextContract) {
    // const data = { product_id: ctx.request.all().id };
    const res = await ProductImage.query()
      .where("product_id", ctx.request.all().id)
      .select();
    return res;
  }

  //Add Image Other from page product/edit/???  เพิ่มรูปภาพ
  public async addImageProductOther(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(data);

    const coverImage = ctx.request.files("image");
    // console.log(coverImage);

    if (coverImage) {
      coverImage.map(async (dataImage) => {
        const res = await this.uploadImageEdit(
          dataImage,
          data.product_id,
          data.main
        );
      });
      return { success: true, message: "เพิ่มรูปสำเร็จ" };
    }
  }
  public async addImageDetailProductOther(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(data);

    const coverImage = ctx.request.files("image");
    // console.log(coverImage);

    if (coverImage) {
      coverImage.map(async (dataImage) => {
        const res = await this.uploadImageDetailEdit(
          dataImage,
          data.product_id,
        );
      });
      return { success: true, message: "เพิ่มรูปสำเร็จ" };
    }
  }
  //EDIT DATA PRODUCT
  public async editDataProduct(ctx: HttpContextContract) {
    const data = ctx.request.all();
    await Product.query().where("id", data.id).update({
      name: data.name,
      price: data.price,
      discount: data.discount,
      brand_id: data.brand_id,
      category: data.category,
      status: data.status,
    });
    await ProductDetail.query().where("product_id", data.id).update({
      description: data.description,
    });
    await Stock.query().where("product_id", data.id).update({
      quantity: data.quantity,
    });
    await ProductNewArrival.query().where("product_id", data.id).update({
      newarrival_status: data.newarrival_status,
    });
    await ProductSeo.query().where("product_id", data.id).update({
      meta_title: data.meta_title,
      meta_description: data.meta_description,
      meta_keywords: data.meta_keywords,
    });
    return { success: true, message: "แก้ไขเสร็จแล้ว" };
  }
  //EDIT ONLY STATUS
  public async editStatusProduct(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await Product.query().where("id", data.id).update({
      status: data.status,
    });
    if (res) {
      return { success: true, message: "แก้ไขเสร็จแล้ว" };
    } else {
      return { success: false, message: "แก้ไขไม่สำเร็จเสร็จแล้ว" };
    }
  }
  //EDIT MAIN IMAGE
  public async editMainImage(ctx: HttpContextContract) {
    const data = ctx.request.all();
    await ProductImage.query()
      .where("id", data.id)
      .where("product_id", data.product_id)
      .update({
        main: 1,
      });
    await ProductImage.query()
      .where("id", "!=", data.id)
      .where("product_id", data.product_id)
      .update({
        main: 0,
      });

    return { success: true, message: "เปลี่ยนรูปหลักสำเร็จ" };
  }
  //DELETE PRODUCT ALL
  public async deleteProduct(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await Product.query().where("id", data.id).update({
      status: 0,
      is_delete: 1,
    });
    if (res) {
      return { success: true, message: "ลบสินค้าเสร็จสิ้นแล้ว" };
    } else {
      return { success: false, message: "ลบสินค้าไม่สำเร็จ" };
    }
  }

  //DELETE IMAGE
  public async deleteImage(ctx: HttpContextContract) {
    const data = ctx.request.all();
    await fs.unlink(
      Application.tmpPath("uploads/product/") + data.image,
      async (err) => {
        if (err) {
          console.log("failed to delete local image:" + err);
        }
      }
    );
    await ProductImage.query().where("id", data.id).delete();
    return { success: true, message: "ลบรูปภาพสำเร็จ" };
  }
  //DELETE IMAGE DETAIL
  public async deleteImageDetail(ctx: HttpContextContract) {
    const data = ctx.request.all();
    await fs.unlink(
      Application.tmpPath("uploads/product/description/") + data.image,
      async (err) => {
        if (err) {
          console.log("failed to delete local image:" + err);
        }
      }
    );
    await ProductDescriptionImage.query().where("id", data.id).delete();
    return { success: true, message: "ลบรูปภาพสำเร็จ" };
  }





  //front end seoul good
  ///
  // public async getProductSeo(ctx: HttpContextContract) {
  //   const { name } = ctx.request.params()
  //   console.log(name);
  // }
  public async getProductOtherSingle(ctx: HttpContextContract) {
    const data = ctx.request.all()
    const res = await Database.from('product_others').where('id', data.product_other_id).first()
    return { success: true, data: res }
  }
  public async getProductOtherData(ctx: HttpContextContract) {
    const data = ctx.request.all()
    if (data.twin) {
      const res = await Database.from('product_others').where('product_id', data.product_id)
        .where('color', data.color).where('size', data.size).first()
      if (res) {
        // res.discount_promotion_other = null
        const promotion = await Database.from('promotion_products').where('product_id', data.product_id).first()
        if (promotion) {
          const parse = JSON.parse(promotion.product_other)
          for (let i = 0; i < parse.length; i++) {
            if (res.id == parse[i].product_other_id) {
              res.discount_promotion = parseInt(parse[i].discount_other_promotion)
            }
          }
        }
        // console.log(res);

        if (res.quantity >= 1) {
          const image = await Database.from('product_other_images').where('product_other_id', res.id)
          return { success: true, data: res, image: image, msg: "มีข้อมูล" }
        } else {
          return { success: false, data: res, msg: "สินค้าหมด" }
        }
      } else {
        return { success: false, msg: "ไม่มีสินค้า" }
      }
    } else if (data.color || data.color && !data.size) {
      const res = await Database.from('product_others').where('product_id', data.product_id)
        .where('color', data.color).first()
      if (res) {
        // res.discount_promotion_other = null
        const promotion = await Database.from('promotion_products').where('product_id', data.product_id).first()

        if (promotion) {
          const parse = JSON.parse(promotion.product_other)
          for (let i = 0; i < parse.length; i++) {
            if (res.id == parse[i].product_other_id) {
              res.discount_promotion = parseInt(parse[i].discount_other_promotion)
            }
          }
        }
        if (res) {
          if (res.quantity >= 1) {
            const image = await Database.from('product_other_images').where('product_other_id', res.id)
            return { success: true, data: res, image: image, msg: "มีข้อมูล" }
          } else {
            return { success: false, data: res, msg: "สินค้าหมด" }
          }
        } else {
          return { success: false, msg: "ไม่มีสินค้า" }
        }
      }
    } else if (data.size || !data.color && data.size) {
      // console.log(5555);

      const res = await Database.from('product_others').where('product_id', data.product_id)
        .where('size', data.size).first()
      if (res) {
        // res.discount_promotion_other = null
        const promotion = await Database.from('promotion_products').where('product_id', data.product_id).first()
        if (promotion) {
          const parse = JSON.parse(promotion.product_other)
          for (let i = 0; i < parse.length; i++) {
            if (res.id == parse[i].product_other_id) {
              res.discount_promotion = parseInt(parse[i].discount_other_promotion)
            }
          }
        }
        if (res) {
          if (res.quantity >= 1) {
            const image = await Database.from('product_other_images').where('product_other_id', res.id)
            return { success: true, data: res, image: image, msg: "มีข้อมูล" }
          } else {
            return { success: false, data: res, msg: "สินค้าหมด" }
          }
        } else {
          return { success: false, msg: "ไม่มีสินค้า" }
        }
      }
    }
  }
  //FrontEnd Search Product
  public async searchProduct(ctx: HttpContextContract) {
    const res = await Database.from('products as a').where('a.status', 1)
      .join('product_details as b', 'a.id', 'b.product_id')
      .join('product_images as c', 'a.id', 'c.product_id').where('c.main', 1)
      .select('a.*', 'b.*', 'b.id as id_detail', 'c.*', 'c.id as id_image')
    return res
  }
  public async searchProductPage(ctx: HttpContextContract) {
    const tableProduct = await Database.from('products as a').where('a.status', 1)
      .join('product_images as b', 'a.id', 'b.product_id').where('b.main', 1)
      .join('product_details as c', 'a.id', 'c.product_id')
      .join('product_new_arrivals as d', 'a.id', 'd.product_id')
      .join('product_seos as e', 'a.id', 'e.product_id')
      .join('stocks as f', 'a.id', 'f.product_id')
    const tableProductOther = await Database.from('product_others')
    tableProduct.map(edit => {
      edit.pdo = []
      for (let i = 0; i < tableProductOther.length; i++) {
        if (edit.id == tableProductOther[i].product_id) {
          edit.pdo.push(tableProductOther[i])
        }
      }
    })
    if (tableProduct.length >= 1) {
      return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
    } else {
      return { success: false, message: 'ดึงข้อมูลได้!!!' }
    }
  }
  //Backend seoulgood product
  public async getProductOtherSingleBackend(ctx: HttpContextContract) {
    const id = ctx.request.all().id
    const res = await Database.from('product_others as a').where('id', id).first()
    const tablePdo = await Database.from('product_other_images').where('product_other_id', res.id)
    return { success: true, data: res, image: tablePdo }
  }
  public async editProductOtherSingleBackend(ctx: HttpContextContract) {
    const data = ctx.request.all()
    console.log(data);
    const regex = /\s/g;
    const res = await ProductOther.query().where('id', data.id).update({
      price: data.price,
      discount: data.discount,
      color: data.color,
      code_color: data.code_color,
      size: data.size,
      code_size: data.size.replace(regex, "_"),
      quantity: data.quantity,
    })
    const coverImage = ctx.request.files("image");
    if (coverImage) {
      coverImage.map(async (dataImage, index) => {
        await this.uploadImageProductOther(dataImage, data.id);
      });
      return { success: true, message: 'แก้ไขสำเร็จ' }
    } else {
      return { success: true, message: 'แก้ไขสำเร็จ' }
    }
  }
  public async DeletePicturePdo(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data);

    const res = await ProductOtherImage.query().where('id', data.id).delete()
    if (res) {
      await fs.unlink(
        Application.tmpPath("uploads/product/other/") + data.image,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
      return { success: true, message: 'ลบรูปภาพสำเร็จแล้ว' }

    }

  }


}
