import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
("use strict");
import Stock from "App/Models/Stock";
export default class StocksController {
  public async index(ctx: HttpContextContract) {
    const data = ctx.request.all();
    if (data) {
      await Stock.create(data);
      return "สร้าง Stock สำเร็จ";
    } else {
      return "สร้าง Stock ไม่สำเร็จ";
    }
  }

  public async editQuantility(ctx: HttpContextContract) {
    const data = {
      product_id: ctx.request.all().id,
      quantity: ctx.request.all().quantity,
    };
    const res = await Stock.query()
      .where("product_id", data.product_id)
      .update({
        quantity: data.quantity,
      });
    if (res) {
      return { success: true, message: "แก้ไข้สถานะสำเร็จ" };
    } else {
      return { success: false, message: "แก้ไข้สถานะไม่สำเร็จ" };
    }
  }
}
