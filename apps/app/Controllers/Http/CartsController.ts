import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
("use strict");
import Cart from "App/Models/Cart";
import Database from "@ioc:Adonis/Lucid/Database";

export default class CartsController {
  public async addItemToCart(ctx: HttpContextContract) {
    const data = ctx.request.all();

    const { id } = ctx.auth.user?.toJSON();
    if (data.color) {
      if (data.size) {
        const pdo = await Database.from('product_others')
          .where('product_id', data.product_id)
          .where('color', data.color)
          .where('size', data.size).first()

        if (pdo.quantity >= 1) {
          let tableProductCheckOther = await Database.from("carts")
            .where("customer_id", data.customer_id)
            .where("product_id", data.product_id)
            .where("product_other_id", pdo.id)
            .first();

          if (tableProductCheckOther) {
            const quantity = await Database.from("carts")
              .select("quantity")
              .where("customer_id", data.customer_id)
              .where("product_id", data.product_id)
              .where("product_other_id", pdo.id)
              .first();
            await Cart.query()
              .where("customer_id", data.customer_id)
              .where("product_id", data.product_id)
              .update({
                quantity: quantity.quantity + data.quantity,
              });
            const res = await Database.from("carts as a")
              .where("customer_id", id)
              .join("products as b", "a.product_id", "b.id")
              .join("product_details as c", "a.product_id", "c.product_id")
              .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
              .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
              .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
              .select("f.price as price_other_product",
                "f.discount as discount_other_product",
                "g.product_other as discount_product_other",
                "g.discount as discount_promotion",
                "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
            for (let i = 0; i < res.length; i++) {
              res[i].colorAndSize = []
              if (res[i].product_other_id) {
                let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
                res[i].colorAndSize.push(coAndSi)
              }
            }
            if (res) {
              for (let i = 0; i < res.length; i++) {
                if (res[i].discount_product_other) {
                  const parse = JSON.parse(res[i].discount_product_other)
                  // console.log(parse.length);
                  for (let b = 0; b < parse.length; b++) {
                    if (res[i].product_other_id == parse[b].product_other_id) {
                      res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                    }
                  }
                }

              }
            }
            return {
              success: true,
              message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
              data: res,
            };
          } else {
            await Cart.create({ customer_id: data.customer_id, product_id: data.product_id, product_other_id: pdo.id, quantity: 1 });
            const res = await Database.from("carts as a")
              .where("customer_id", id)
              .join("products as b", "a.product_id", "b.id")
              .join("product_details as c", "a.product_id", "c.product_id")
              .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
              .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
              .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
              .select("f.price as price_other_product",
                "f.discount as discount_other_product",
                "g.product_other as discount_product_other",
                "g.discount as discount_promotion",
                "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
            for (let i = 0; i < res.length; i++) {
              res[i].colorAndSize = []
              if (res[i].product_other_id) {
                let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
                res[i].colorAndSize.push(coAndSi)
              }
            }
            if (res) {
              for (let i = 0; i < res.length; i++) {
                if (res[i].discount_product_other) {
                  const parse = JSON.parse(res[i].discount_product_other)
                  // console.log(parse.length);
                  for (let b = 0; b < parse.length; b++) {
                    if (res[i].product_other_id == parse[b].product_other_id) {
                      res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                    }
                  }
                }

              }
            }
            return {
              success: true,
              message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
              data: res,
            };
          }
        } else {
          return {
            success: false,
            message: "สินค้าหมด",
          };
        }
      }
      else {
        const pdo = await Database.from('product_others').where('product_id', data.product_id).where('color', data.color).first()
        // console.log(pdo);

        if (pdo.quantity >= 1) {
          let tableProductCheckOther = await Database.from("carts")
            .where("customer_id", data.customer_id)
            .where("product_id", data.product_id)
            .where("product_other_id", pdo.id)
            .first();
          // console.log(tableProductCheckOther);
          if (tableProductCheckOther) {
            const quantity = await Database.from("carts")
              .select("quantity")
              .where("customer_id", data.customer_id)
              .where("product_id", data.product_id)
              .where("product_other_id", pdo.id)
              .first();
            await Cart.query()
              .where("customer_id", data.customer_id)
              .where("product_id", data.product_id)
              .update({
                quantity: quantity.quantity + data.quantity,
              });
            const res = await Database.from("carts as a")
              .where("customer_id", id)
              .join("products as b", "a.product_id", "b.id")
              .join("product_details as c", "a.product_id", "c.product_id")
              .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
              .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
              .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
              .select("f.price as price_other_product",
                "f.discount as discount_other_product",
                "g.product_other as discount_product_other",
                "g.discount as discount_promotion",
                "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
            for (let i = 0; i < res.length; i++) {
              res[i].colorAndSize = []
              if (res[i].product_other_id) {
                let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
                res[i].colorAndSize.push(coAndSi)
              }
            }
            if (res) {
              for (let i = 0; i < res.length; i++) {
                if (res[i].discount_product_other) {
                  const parse = JSON.parse(res[i].discount_product_other)
                  // console.log(parse.length);
                  for (let b = 0; b < parse.length; b++) {
                    if (res[i].product_other_id == parse[b].product_other_id) {
                      res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                    }
                  }
                }

              }
            }
            return {
              success: true,
              message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
              data: res,
            };
          } else {
            await Cart.create({ customer_id: data.customer_id, product_id: data.product_id, product_other_id: pdo.id, quantity: 1 });
            const res = await Database.from("carts as a")
              .where("customer_id", id)
              .join("products as b", "a.product_id", "b.id")
              .join("product_details as c", "a.product_id", "c.product_id")
              .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
              .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
              .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
              .select("f.price as price_other_product",
                "g.product_other as discount_product_other",
                "g.discount as discount_promotion",
                "f.discount as discount_other_product",
                "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
            for (let i = 0; i < res.length; i++) {
              res[i].colorAndSize = []
              if (res[i].product_other_id) {
                let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
                res[i].colorAndSize.push(coAndSi)
              }
            }
            if (res) {
              for (let i = 0; i < res.length; i++) {
                if (res[i].discount_product_other) {
                  const parse = JSON.parse(res[i].discount_product_other)
                  // console.log(parse.length);
                  for (let b = 0; b < parse.length; b++) {
                    if (res[i].product_other_id == parse[b].product_other_id) {
                      res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                    }
                  }
                }

              }
            }
            return {
              success: true,
              message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
              data: res,
            };
          }

        }
      }
    }
    else if (data.size) {
      const pdo = await Database.from('product_others').where('product_id', data.product_id).where('size', data.size).first()
      if (pdo.quantity >= 1) {
        let tableProductCheckOther = await Database.from("carts")
          .where("customer_id", data.customer_id)
          .where("product_id", data.product_id)
          .where("product_other_id", pdo.id)
          .first();
        // console.log(tableProductCheckOther);
        if (tableProductCheckOther) {
          const quantity = await Database.from("carts")
            .select("quantity")
            .where("customer_id", data.customer_id)
            .where("product_id", data.product_id)
            .where("product_other_id", pdo.id)
            .first();
          await Cart.query()
            .where("customer_id", data.customer_id)
            .where("product_id", data.product_id)
            .update({
              quantity: quantity.quantity + data.quantity,
            });
          const res = await Database.from("carts as a")
            .where("customer_id", id)
            .join("products as b", "a.product_id", "b.id")
            .join("product_details as c", "a.product_id", "c.product_id")
            .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
            .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select("f.price as price_other_product",
              "g.product_other as discount_product_other",
              "g.discount as discount_promotion",
              "f.discount as discount_other_product",
              "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
          for (let i = 0; i < res.length; i++) {
            res[i].colorAndSize = []
            if (res[i].product_other_id) {
              let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
              res[i].colorAndSize.push(coAndSi)
            }
          }
          if (res) {
            for (let i = 0; i < res.length; i++) {
              if (res[i].discount_product_other) {
                const parse = JSON.parse(res[i].discount_product_other)
                // console.log(parse.length);
                for (let b = 0; b < parse.length; b++) {
                  if (res[i].product_other_id == parse[b].product_other_id) {
                    res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                  }
                }
              }

            }
          }
          return {
            success: true,
            message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
            data: res,
          };
        } else {
          await Cart.create({ customer_id: data.customer_id, product_id: data.product_id, product_other_id: pdo.id, quantity: 1 });
          const res = await Database.from("carts as a")
            .where("customer_id", id)
            .join("products as b", "a.product_id", "b.id")
            .join("product_details as c", "a.product_id", "c.product_id")
            .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
            .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select("f.price as price_other_product",
              "g.product_other as discount_product_other",
              "g.discount as discount_promotion",
              "f.discount as discount_other_product",
              "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
          for (let i = 0; i < res.length; i++) {
            res[i].colorAndSize = []
            if (res[i].product_other_id) {
              let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
              res[i].colorAndSize.push(coAndSi)
            }
          }
          if (res) {
            for (let i = 0; i < res.length; i++) {
              if (res[i].discount_product_other) {
                const parse = JSON.parse(res[i].discount_product_other)
                // console.log(parse.length);
                for (let b = 0; b < parse.length; b++) {
                  if (res[i].product_other_id == parse[b].product_other_id) {
                    res[i].discount_promotion = parseInt(parse[b].discount_other_promotion)
                  }
                }
              }

            }
          }
          return {
            success: true,
            message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
            data: res,
          };
        }

      } else {
        return {
          success: false,
          message: "สินค้าหมด",
        };
      }

    }
    else {
      let tableProduct = await Database.from("carts")
        .where("customer_id", data.customer_id)
        .where("product_id", data.product_id)
        .first();
      if (tableProduct) {
        const quantity = await Database.from("carts")
          .select("quantity")
          .where("customer_id", data.customer_id)
          .where("product_id", data.product_id)
          .first();
        await Cart.query()
          .where("customer_id", data.customer_id)
          .where("product_id", data.product_id)
          .update({
            quantity: quantity.quantity + data.quantity,
          });
        const res = await Database.from("carts as a")
          .where("customer_id", id)
          .join("products as b", "a.product_id", "b.id")
          .join("product_details as c", "a.product_id", "c.product_id")
          .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
          .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
          .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
          .select("f.price as price_other_product",
            "g.product_other as discount_product_other",
            "g.discount as discount_promotion",
            "f.discount as discount_other_product",
            "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
        for (let i = 0; i < res.length; i++) {
          res[i].colorAndSize = []
          if (res[i].product_other_id) {
            let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
            res[i].colorAndSize.push(coAndSi)
          }
        }

        return {
          success: true,
          message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
          data: res,
        };
      } else {
        await Cart.create(data);
        const res = await Database.from("carts as a")
          .where("customer_id", id)
          .join("products as b", "a.product_id", "b.id")
          .join("product_details as c", "a.product_id", "c.product_id")
          .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
          .leftJoin('product_others as f', 'f.id', 'a.product_other_id')
          .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
          .select("f.price as price_other_product",
            "f.discount as discount_other_product",
            "g.product_other as discount_product_other",
            "g.discount as discount_promotion",
            "a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
        for (let i = 0; i < res.length; i++) {
          res[i].colorAndSize = []
          if (res[i].product_other_id) {
            let coAndSi = await Database.from('product_others').where('id', res[i].product_other_id).first()
            res[i].colorAndSize.push(coAndSi)
          }
        }
        return {
          success: true,
          message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
          data: res,
        };
      }
    }
    // const { id } = ctx.auth.user?.toJSON();


  }
  public async inCartBackend(ctx: HttpContextContract) {
    const { id } = ctx.request.params();
    const res = Database.from("carts as a")
      .where("customer_id", id)
      .join("products as b", "a.product_id", "b.id");
    return res;
  }
}
