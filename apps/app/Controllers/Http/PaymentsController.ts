import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Payment from "App/Models/Payment";
import Stock from "App/Models/Stock";
import ProductSold from "App/Models/ProductSold";
import Province from "App/Models/Province";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
import Database from "@ioc:Adonis/Lucid/Database";
import ProductOther from "App/Models/ProductOther";
const fs = require("fs");
("use strict");

export default class PaymentsController {
  public async imageSlipShow(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/payment/slip/" + name)
    );
    ctx.response.stream(getStream);
  }

  public async getPictureSlip(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data.order_number);
    const res = await Database.from('payments').where('order_number', data.order_number).first()
    return { success: true, data: res }

  }
  public async changePictureSlip(ctx: HttpContextContract) {
    const data = ctx.request.all()
    const image = ctx.request.file('image')
    const d = new Date();
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    const order_number = `${addZero(d.getSeconds())}${addZero(
      d.getMinutes()
    )}${addZero(d.getHours())}${d.getDate()}${d.getMonth() + 1
      }${d.getFullYear()}`;
    const newName = order_number.toString() + "slip.jpg"
    if (image) {
      const res = await Payment.query().where('order_number', data.order_number).update({
        slip_image: newName
      })

      if (res) {
        await fs.unlink(
          Application.tmpPath("uploads/payment/slip/") + data.image_name,
          async (err) => {
            if (err) {
              console.log("failed to delete local image:" + err);
            }
          }
        );
        await image.move(Application.tmpPath("uploads/payment/slip"), {
          name: newName,
          overwrite: true,
        });
        return {success:true , msg:'เปลี่ยนรูปเรียบร้อย'}
      }

    }


  }



  public async placeOrder(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const { id } = ctx.auth.user?.toJSON();
    const image = ctx.request.file("image");
    const d = new Date();
    function addZero(i) {
      if (i < 10) {
        i = "0" + i;
      }
      return i;
    }
    const order_number = `${addZero(d.getSeconds())}${addZero(
      d.getMinutes()
    )}${addZero(d.getHours())}${d.getDate()}${d.getMonth() + 1
      }${d.getFullYear()}`;
    const newName = order_number.toString() + "slip.jpg"
    if (image) {
      await image.move(Application.tmpPath("uploads/payment/slip"), {
        name: newName,
        overwrite: true,
      });
      const res = await Payment.create({
        customer_id: data.customer_id,
        order_number: order_number,
        status: data.status,
        cut_order: 1,
        product_order: data.product_order,
        address: data.address,
        slip_image: newName,
        price: data.sumPrice,
      });

      if (res) {
        const lastStep = await Database.from("carts")
          .where("customer_id", data.customer_id)
          .delete();
        if (lastStep) {
          const updateCart = await Database.from("carts as a")
            .where("customer_id", data.customer_id)
            .join("products as b", "a.product_id", "b.id")
            .join("product_details as c", "a.product_id", "c.product_id")
            .join("product_images as d", "a.product_id", "d.product_id").where('d.main', 1)
            .select("a.*", "a.id as cart_id", "b.*", "c.*", "d.*");
          return {
            success: true,
            message: "เพิ่มสิ้นค้าในตะกร้าเรียบร้อย",
            data: updateCart,
            order_number: order_number,
          };
        }
      }
    }

    // const res = await Payment.create(data)
  }
  public async addressForPlace(ctx: HttpContextContract) {
    const { id } = ctx.auth.user?.toJSON();
    const res = await Database.from('customer_addresses').where('customer_id', id)
    return { success: true, data: res }
  }
  public async provinceSelect(ctx: HttpContextContract) {
    const res = await Province.query();
    return res;
  }
  public async paymentOrderSelect(ctx: HttpContextContract) {
    const { id } = ctx.auth.user?.toJSON();
    const tablePayment = await Database.from("payments").where(
      "customer_id",
      id
    ).orderBy('created_at', 'desc');
    return { success: true, data: tablePayment, message: "ดึงข้อมูลสำเร็จ" };
  }

  public async getProductAndAdress(ctx: HttpContextContract) {
    const data = ctx.request.all()
    const tableAddress = await Database.from('customer_addresses').where('id', data.address).first()
    return { success: true, address: tableAddress }
  }
  public async getPaymentBackend(ctx: HttpContextContract) {
    const res = await Payment.query().orderBy('created_at', 'desc');
    return res;
  }
  public async editStatusPayment(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(data);

    //Status 1 = อยู่ระหว่างตรวจสอบ
    //Status 2 = ชำระเงินเรียบร้อย เตรียมจัดส่ง
    //Status 3 = อยู่ระหว่างดำเนินการจัดส่ง
    //Status 4 = ยกเลิกคำสั่งซื้อ
    const status = 0;
    const checklist = []
    const data_missing = []
    if (data.status == 0 || data.status == 1 || data.status == 2) {
      for (let i = 0; i < data.product_order.length; i++) {
        if (data.product_order[i].product_other_id) {
          let storeOther = await Database.from('product_others').where('id', data.product_order[i].product_other_id).first()
          if (data.product_order[i].product_other_id == storeOther.id) {
            if (data.product_order[i].quantity <= storeOther.quantity) {
              checklist.push(data.product_order[i])
              // console.log(checklist.length);
              if (checklist.length == data.product_order.length) {
                await Payment.query().where("id", data.id).update({
                  status: 2,
                  cut_order: 0,
                });
                return { success: true, message: "ตรวจสอบสินค้าเสร็จสิ้น" }
              }
            }
            else {
              data_missing.push(data.product_order[i].name)
              await Payment.query().where("id", data.id).update({
                status: 4,
              });
              if (data_missing.length > 1) {
                return { message: `สินค้า ${data_missing.toString()} ไม่เพียงพอ`, status: 4 }
              } else {
                return { message: `สินค้า ${data.product_order[i].name} ${data.product_order[i].color ? data.product_order[i].color : ''} ${data.product_order[i].size != 0 ? data.product_order[i].size : ''}  ไม่เพียงพอ`, status: 4 }
              }

            }
          } else {
            return { message: 'ไม่พบสินค้า', product_name: data.product_order[i].name }
          }
        } else {
          let store = await Database.from('stocks').where('product_id', data.product_order[i].product_id).first()
          if (data.product_order[i].product_id == store.product_id) {
            if (data.product_order[i].quantity <= store.quantity) {
              checklist.push(data.product_order[i])
              // console.log(checklist.length);
              if (checklist.length == data.product_order.length) {
                await Payment.query().where("id", data.id).update({
                  status: 2,
                  cut_order: 0,
                });
                return { success: true, message: "ตรวจสอบสินค้าเสร็จสิ้น" }
              }
            }
            else {
              data_missing.push(data.product_order[i].name)
              await Payment.query().where("id", data.id).update({
                status: 4,
              });
              if (data_missing.length > 1) {
                return { message: `สินค้า ${data_missing.toString()} ไม่เพียงพอ`, status: 4 }
              } else {
                return { message: `สินค้า ${data.product_order[i].name} ไม่เพียงพอ`, status: 4 }
              }

            }
          } else {
            return { message: 'ไม่พบสินค้า', product_name: data.product_order[i].name }
          }
        }

      }
    } else if (data.status == 4) {
      if (data.cut_order == 2) {
        for (let i = 0; i < data.product_order.length; i++) {
          if (data.product_order[i].product_other_id) {
            let productQuantity = await Database.from("product_others").where("id", data.product_order[i].product_other_id)
              .first();
            let productSold = await Database.from("product_solds")
              .where("product_id", data.product_order[i].product_id)
              .first();
            await ProductOther.query()
              .where("product_id", data.product_order[i].product_id)
              .update({
                quantity: productQuantity.quantity + data.product_order[i].quantity,
              });
            if (productSold) {
              await ProductSold.query()
                .where("product_id", data.product_order[i].product_id)
                .update({
                  quantity: productSold.quantity - data.product_order[i].quantity,
                });
            }
          } else {
            let productQuantity = await Database.from("stocks").where("product_id", data.product_order[i].product_id)
              .first();
            await Stock.query()
              .where("product_id", data.product_order[i].product_id)
              .update({
                quantity: productQuantity.quantity + data.product_order[i].quantity,
              });
            let productSold = await Database.from("product_solds")
              .where("product_id", data.product_order[i].product_id)
              .first();
            if (productSold) {
              await ProductSold.query()
                .where("product_id", data.product_order[i].product_id)
                .update({
                  quantity: productSold.quantity - data.product_order[i].quantity,
                });
            }
          }

        }
        const res = await Payment.query().where("id", data.id).update({
          status: 4,
          cut_order: 4,
        });
        if (res) {
          return { message: `ยกเลิกสินค้าเรียบร้อย`, status: 4 }
        } else {
          return { message: `ยกเลิกสินค้าไม่ได้` }
        }
      } else {
        const res = await Payment.query().where("id", data.id).update({
          status: 4,
          cut_order: 4,
        });
        if (res) {
          return { message: `ยกเลิกสินค้าเรียบร้อย`, status: 4 }
        } else {
          return { message: `ยกเลิกสินค้าไม่ได้`, status: 4 }
        }
      }

    } else {
      for (let i = 0; i < data.product_order.length; i++) {
        let store = await Database.from('stocks').where('product_id', data.product_order[i].product_id).first()
        if (data.product_order[i].product_id == store.product_id) {
          if (data.product_order[i].quantity <= store.quantity) {
            checklist.push(data.product_order[i])
            // console.log(checklist.length);
            if (checklist.length == data.product_order.length) {
              await Payment.query().where("id", data.id).update({
                status: 3,
                cut_order: 2,
              });
              return { success: true, message: "เปลี่ยนเป็นสถานะเตรียมส่งเรียบร้อย" }
            }
          }
          else {
            data_missing.push(data.product_order[i].name)
            await Payment.query().where("id", data.id).update({
              status: 4,
            });
            if (data_missing.length > 1) {

              return { message: `สินค้า ${data_missing.toString()} ไม่เพียงพอ`, status: 4 }

            } else {
              return { message: `สินค้า ${data.product_order[i].name} ไม่เพียงพอ`, status: 4 }
            }

          }
        } else {
          return { message: 'ไม่พบสินค้า', product_name: data.product_order[i].name }
        }
      }
    }


    //   const res = await Payment.query().where("id", data.id).update({
    //     status: data.status,
    //   });
    //   if (res) {
    //     if (data.status == 2) {

    //       for (let i = 0; i < data.product_order.length; i++) {
    //         let productQuantity = await Database.from("stocks")
    //           .where("product_id", data.product_order[i].product_id)
    //           .first();
    //         // console.log(data.product_order[i]);

    //         let productSold = await Database.from("product_solds")
    //           .where("product_id", data.product_order[i].product_id)
    //           .first();
    //         if (productSold) {
    //           await ProductSold.query()
    //             .where("product_id", data.product_order[i].product_id)
    //             .update({
    //               quantity: productSold.quantity + data.product_order[i].quantity,
    //             });
    //         } else {
    //           await ProductSold.create({
    //             name: data.product_order[i].name,
    //             product_id: data.product_order[i].product_id,
    //             quantity: data.product_order[i].quantity,
    //           });
    //         }
    //         const updateStock = await Stock.query()
    //           .where("product_id", data[i].product_id)
    //           .update({
    //             quantity: productQuantity.quantity - data[i].quantity,
    //           });
    //       }
    //     }
    //     return { success: true, message: "แก้ข้อมูลสำเร็จแล้ว" };
    //   } else {
    //     return { success: false, message: "แก้ข้อมูลไม่สำเร็จแล้ว" };
    //   }
  }
  public async addNoteProduct(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data[0].id);

    const res = await Payment.query().where('id', data[0].id).update({
      product_order: JSON.stringify(data[0].product_order)
    })
    if (res) {
      return { success: true, message: "แก้ไขข้อมูลสำเร็จแล้ว" };
    } else {
      return { success: false, message: "แก้ไขข้อมูลไม่สำเร็จแล้ว" };
    }

  }
  public async cutProductInStock(ctx: HttpContextContract) {
    const data = ctx.request.all()
    // console.log(data);

    if (data.status == 2) {
      for (let i = 0; i < data.product_order.length; i++) {
        if (data.product_order[i].product_other_id) {
          let productQuantity = await Database.from("product_others")
            .where("id", data.product_order[i].product_other_id)
            .first();
          let productSold = await Database.from("product_solds")
            .where("product_id", data.product_order[i].product_id)
            .first();
          if (productSold) {
            await ProductSold.query()
              .where("product_id", data.product_order[i].product_id)
              .update({
                quantity: productSold.quantity + data.product_order[i].quantity,
              });
          } else {
            await ProductSold.create({
              name: data.product_order[i].name,
              product_id: data.product_order[i].product_id,
              quantity: data.product_order[i].quantity,
            });
          }
          if (productQuantity.quantity) {
            const res = await ProductOther.query()
              .where("id", data.product_order[i].product_other_id)
              .update({
                quantity: productQuantity.quantity - data.product_order[i].quantity,
              });
            // if (res) {
            //   await Payment.query().where("id", data.id).update({
            //     cut_order: 2,
            //     status: 3,
            //   });
            //   return { success: true, message: "ตัดสินค้าสำเร็จแล้ว" };
            // } else {
            //   return { success: false, message: "ตัดสินค้าไม่สำเร็จแล้ว" };
            // }
          }
        }
        else {
          let productQuantity = await Database.from("stocks")
            .where("product_id", data.product_order[i].product_id)
            .first();
          let productSold = await Database.from("product_solds")
            .where("product_id", data.product_order[i].product_id)
            .first();
          if (productSold) {
            await ProductSold.query()
              .where("product_id", data.product_order[i].product_id)
              .update({
                quantity: productSold.quantity + data.product_order[i].quantity,
              });
          } else {
            await ProductSold.create({
              name: data.product_order[i].name,
              product_id: data.product_order[i].product_id,
              quantity: data.product_order[i].quantity,
            });
          }
          if (productQuantity.quantity) {
            const res = await Stock.query()
              .where("product_id", data.product_order[i].product_id)
              .update({
                quantity: productQuantity.quantity - data.product_order[i].quantity,
              });
            // if (res) {
            //   await Payment.query().where("id", data.id).update({
            //     cut_order: 2,
            //     status: 3,
            //   });
            //   return { success: true, message: "ตัดสินค้าสำเร็จแล้ว" };
            // } else {
            //   return { success: false, message: "ตัดสินค้าไม่สำเร็จแล้ว" };
            // }
          }
        }


        // if (productQuantity.quantity) {
        //   const res = await Stock.query()
        //     .where("product_id", data.product_order[i].product_id)
        //     .update({
        //       quantity: productQuantity.quantity - data.product_order[i].quantity,
        //     });
        //   if (res) {
        //     await Payment.query().where("id", data.id).update({
        //       cut_order: 2,
        //       status: 3,
        //     });
        //     return { success: true, message: "ตัดสินค้าสำเร็จแล้ว" };
        //   } else {
        //     return { success: false, message: "ตัดสินค้าไม่สำเร็จแล้ว" };

        //   }
        // }
      }
      await Payment.query().where("id", data.id).update({
        cut_order: 2,
        status: 3,
      });
      return { success: true, message: "ตัดสินค้าสำเร็จแล้ว" };
    }
  }
}
