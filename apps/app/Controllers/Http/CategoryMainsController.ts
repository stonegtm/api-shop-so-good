// import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Application from "@ioc:Adonis/Core/Application";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";

("use strict");
import CategoryMain from "App/Models/CategoryMain";
import CategoryList from "App/Models/CategoryList";
export default class CategoryMainsController {
  //อัพโหลด รูปภาพ
  public async imageForUpload(coverImage) {
    //เพิ่มวันที่ต่อหน้าชื่อ
    const addBeforeNameImg = new Date().getTime();

    //สร้างตัวแปรเก็บชื่อที่ต่อกันแล้ว
    const newNameImg = addBeforeNameImg + coverImage.data.clientName;
    // อัพรูปลงเครื่อง api
    await coverImage.move(Application.tmpPath("uploads/categories/cover"), {
      name: newNameImg,
      overwrite: true,
    });
    return { success: true, message: newNameImg };
  }
  //อัพโหลด รูปภาพ
  //โชว์รูปของ Categories หลัก
  public async selectImageMain(ctx: HttpContextContract) {
    //เก็บพารามมาจากหน้า
    const { name } = ctx.request.params();
    //เรียกรูปภาพออกไปโชว์
    const getStream = await Drive.getStream(
      Application.tmpPath("uploads/categories/cover/" + name)
    );
    ctx.response.stream(getStream);
  }
  //โชว์รูปของ Categories หลัก

  ////////////////////////////////////////////////////////////CREATE CATEGORY MAIN START
  public async index(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("img_cover");
    const regex = /\s/g;
    let data = {
      name: ctx.request.all().name,
      code_name: ctx.request.all().name.replace(regex, "_"),
      displayed: ctx.request.all().displayed,
      main: ctx.request.all().main,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
    };
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img_cover = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ

    const res = await CategoryMain.create(data);
    if (res) {
      return { success: true, message: "สร้าง Category สำเร็จ" };
    } else {
      return { success: true, message: "สร้าง Category ไม่สำเร็จ" };
    }
  }
  ////////////////////////////////////////////////////////////CREATE CATEGORY MAIN END
  ////////////////////////////////////////////////////////////SELECT ONLY **NANE** CATEGORY MAIN START
  public async categoriesForSelect(ctx: HttpContextContract) {
    const res = await CategoryMain.query().select();
    return res;
  }
  ////////////////////////////////////////////////////////////SELECT ONLY **NANE** CATEGORY MAIN END
  ///////////////////////////////////////////////////////////SELECT ALL CATEGORIES***
  public async categoriesMain(ctx: HttpContextContract) {
    const res = await CategoryMain.query().select();
    return res;
  }
  ///////////////////////////////////////////////////////////SELECT ALL CATEGORIES***
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES***
  public async selectByid(ctx: HttpContextContract) {
    const id = ctx.request.all().id;
    const res = await CategoryMain.query().where("id", id).select();
    if (res) {
      return res;
    } else {
      return { success: false, message: "ไม่สามารถเปิดได้ในตอนนี้" };
    }
  }
  ///////////////////////////////////////////////////////////SELECT DETAIL BY ID CATEGORIES***
  ///
  ///
  ///EDIT CATEGORIES MAIN
  public async editStatusOnly(ctx: HttpContextContract) {
    const data = ctx.request.all()
    if (data.status_only) {
      const res = await CategoryMain.query().where("id", data.id).update({
        displayed: data.displayed
      })
      if (res) {
        return { success: true, message: "แก้ไขสำเร็จแล้ว" };
      } else {
        return { success: false, message: "แก้ไขไม่สำเร็จแล้ว" };
      }
    }
  }


  public async editCategories(ctx: HttpContextContract) {
    const coverImage = ctx.request.file("newImg");
    const regex = /\s/g;

    let data = {
      id: ctx.request.all().id,
      name: ctx.request.all().name,
      code_name: ctx.request.all().name.replace(regex, "_"),
      displayed: ctx.request.all().displayed ? 1 : 0,
      meta_title: ctx.request.all().meta_title,
      meta_description: ctx.request.all().meta_description,
      meta_keywords: ctx.request.all().meta_keywords,
    };
    const delImg = ctx.request.only(["img_cover"]);
    if (delImg.img_cover != "undefined" && delImg.img_cover != 1) {
      await fs.unlink(
        Application.tmpPath("uploads/categories/cover/") + delImg.img_cover,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }

    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ
    if (coverImage) {
      const waiting = await this.imageForUpload(coverImage);
      if (waiting.success) {
        data.img_cover = waiting.message;
      }
    }
    //เรียกใช้ฟังก์ชั่นเพื่ออัพรูปภาพ

    const res = await CategoryMain.query().where("id", data.id).update({
      name: data.name,
      displayed: data.displayed,
      img_cover: data.img_cover,
      meta_title: data.meta_title,
      meta_description: data.meta_description,
      meta_keywords: data.meta_keywords,
    });
    if (res) {
      return { success: true, message: "แก้ไขสำเร็จแล้ว" };
    } else {
      return { success: false, message: "แก้ไขไม่สำเร็จแล้ว" };
    }
  }
  ///EDIT CATEGORIES MAIN END

  //DELETE CATEGORIES MAIN START
  public async deleteCategories(ctx: HttpContextContract) {
    const data = ctx.request.all();
    // console.log(img_cover);

    if (data.img_cover) {
      await fs.unlink(
        Application.tmpPath("uploads/categories/cover/") + data.img_cover,
        async (err) => {
          if (err) {
            console.log("failed to delete local image:" + err);
          }
        }
      );
    }
    const res = await CategoryMain.query().where("id", data.id).delete();
    if (res) {
      return { success: true, message: "ลบสำเร็จแล้ว" };
    } else {
      return { success: false, message: "ลบไม่สำเร็จแล้ว" };
    }
  }

  public async selectCategoryByName(ctx: HttpContextContract) {
    const data = ctx.request.all();
    const res = await CategoryMain.query().where("id", data.id).select();
    return res;
  }

  public async categoriesForProduct(ctx: HttpContextContract) {
    const tableMain = await Database.from("category_mains as a").select(
      "a.id",
      "a.name"
    );
    const tableList = await Database.from("category_lists as b").select(
      "b.id",
      "b.id_category_main",
      "b.name"
    );
    const data = { tableMain, tableList };
    return data;

    // SELECT * FROM t1
    // LEFT JOIN t2 ON t1.id = t2.id
    // UNION ALL
    // SELECT * FROM t1
    // RIGHT JOIN t2 ON t1.id = t2.id
    // WHERE t1.id IS NULL
  }
}
