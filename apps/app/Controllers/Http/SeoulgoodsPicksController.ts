import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Drive from "@ioc:Adonis/Core/Drive";
const fs = require("fs");
import Database from "@ioc:Adonis/Lucid/Database";
import SeoulgoodsPick from "App/Models/SeoulgoodsPick";
export default class SeoulgoodsPicksController {
    public async index(ctx: HttpContextContract) {
        const res = await Database.from('seoulgoods_picks as a')
            .join('products as b', 'a.product_id', 'b.id')
            .join('product_images as c', 'a.product_id', 'c.product_id')
            .where('c.main', 1)
        return { success: true, data: res }
    }
    public async seoulgoodspickPickedFront(ctx: HttpContextContract) {
        const tableProduct = await Database.from('seoulgoods_picks as a')
            .join('products as aa', 'a.product_id', 'aa.id').where('aa.status', 1)
            .join('product_images as b', 'a.product_id', 'b.product_id').where('b.main', 1)
            .join('product_details as c', 'a.product_id', 'c.product_id')
            .join('product_new_arrivals as d', 'a.product_id', 'd.product_id')
            .join('product_seos as e', 'a.product_id', 'e.product_id')
            .join('stocks as f', 'a.product_id', 'f.product_id')
            .leftJoin('promotion_products as g', 'a.product_id', 'g.product_id')
            .select('a.*', 'aa.*', 'b.*', 'c.*', 'd.*', 'e.*', 'f.*', 'g.discount as discount_promotion',)
        const tableProductOther = await Database.from('product_others')
        tableProduct.map(edit => {
            edit.pdo = []
            for (let i = 0; i < tableProductOther.length; i++) {
                if (edit.id == tableProductOther[i].product_id) {
                    edit.pdo.push(tableProductOther[i])
                }
            }
        })
        if (tableProduct.length >= 1) {
            return { success: true, data: tableProduct, message: 'ดึงข้อมูลได้!!!' }
        } else {
            return { success: false, message: 'ดึงข้อมูลได้!!!' }
        }
    }
    public async addSeoulgoodPick(ctx: HttpContextContract) {
        const data = ctx.request.all()
        if (data.search) {
            const check = await Database.from('seoulgoods_picks').where('product_id', data.data.id).first()
            // console.log(data);
            if (check) {
                return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
            } else {
                const res = await SeoulgoodsPick.create({ product_id: data.data.id })
                if (res) {
                    return { success: true, message: 'เพิ่มเรียบร้อย' }
                } else {
                    return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
                }
            }
        } else if (data.select) {
            const check = await Database.from('seoulgoods_picks').where('product_id', data.data.product_id).first()
            // console.log(check);
            if (check) {
                return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
            } else {
                const res = await SeoulgoodsPick.create({ product_id: data.data.product_id })
                if (res) {
                    return { success: true, message: 'เพิ่มเรียบร้อย' }
                } else {
                    return { success: false, message: 'มีสินค้าตัวนี้แล้ว' }
                }
            }
        }
    }
    public async deleteSeoulgoodspick(ctx: HttpContextContract) {
        const data = ctx.request.all()
        const res = await SeoulgoodsPick.query().where('product_id', data.product_id).delete()
        if (res) {
            return { success: true, message: 'ลบสำเร็จแล้ว' }
        } else {
            return { success: false, message: 'ลบไม่สำเร็จแล้ว' }
        }
    }

}


