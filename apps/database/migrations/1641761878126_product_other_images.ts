import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductOtherImages extends BaseSchema {
  protected tableName = 'product_other_images'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_other_id')
      table.string('image')
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
