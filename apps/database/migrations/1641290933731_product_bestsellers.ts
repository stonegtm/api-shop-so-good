import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductBestsellers extends BaseSchema {
  protected tableName = 'product_bestsellers'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("rating", 4);
      table.integer('product_id')
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
