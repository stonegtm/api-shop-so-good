import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductReviewImages extends BaseSchema {
  protected tableName = 'product_review_images'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('customer_id', 11)
      table.integer('product_review_id', 11)
      table.integer('product_id', 11)
      table.string('image_review', 255)


      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
