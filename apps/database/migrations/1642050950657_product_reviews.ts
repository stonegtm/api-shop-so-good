import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductReviews extends BaseSchema {
  protected tableName = 'product_reviews'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('customer_id', 11)
      table.integer('product_id', 11)
      table.integer('star', 1)
      table.string('review_description', 255)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
