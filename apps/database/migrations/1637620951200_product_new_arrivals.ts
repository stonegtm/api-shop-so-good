import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductNewArrivals extends BaseSchema {
  protected tableName = 'product_new_arrivals'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.integer("rating", 11);
      table.boolean('newarrival_status')
      table.boolean('is_delete')
       table.timestamps();
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
