import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductDescriptionImages extends BaseSchema {
  protected tableName = 'product_description_images'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.string('image', 255)

      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
