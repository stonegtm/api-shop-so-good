import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Employees extends BaseSchema {
  protected tableName = "employees";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("username");
      table.string("password");
      table.integer("rules");
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
