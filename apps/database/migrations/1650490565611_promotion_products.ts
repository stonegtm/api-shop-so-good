import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PromotionProducts extends BaseSchema {
  protected tableName = 'promotion_products'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('promotion_id')
      table.integer('product_id')
      table.text('product_other')
      table.integer('discount')
      table.boolean("status");
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
