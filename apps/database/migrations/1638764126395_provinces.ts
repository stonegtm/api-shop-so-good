import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Provinces extends BaseSchema {
  protected tableName = 'provinces'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string("code", 2);
      table.string("name_th", 150);
      table.string("name_en", 150);
      table.integer("geography_id", 5);

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps();

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
