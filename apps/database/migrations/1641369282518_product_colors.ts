import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductColors extends BaseSchema {
  protected tableName = 'product_colors'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.string('color',100)
      table.string('code_color',100)
      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
