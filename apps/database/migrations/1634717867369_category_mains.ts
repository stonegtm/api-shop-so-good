import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CategoryMains extends BaseSchema {
  protected tableName = "category_mains";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id").primary();
      table.string("name");
      table.string("code_name");
      table.boolean("displayed");
      table.boolean("main");
      table.string("img_cover");
      table.string("meta_title", 70);
      table.string("meta_description", 160);
      table.string("meta_keywords", 255);
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
