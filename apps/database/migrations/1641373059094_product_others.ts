import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductOthers extends BaseSchema {
  protected tableName = 'product_others'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.decimal("price", 10, 2);
      table.decimal("discount", 10, 2);
      table.string('color', 100)
      table.string('code_color', 100)
      table.string('size', 200)
      table.string('code_size', 200)
      table.integer('quantity')
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
