import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Payments extends BaseSchema {
  protected tableName = 'payments'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("customer_id", 11);
      table.integer("status", 1);
      table.integer("cut_order",1);
      table.string("order_number", 255);
      table.integer("address", 3);
      table.text("product_order");
      table.string("slip_image", 255);
      table.decimal("price", 10, 2);
       table.timestamps();

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
