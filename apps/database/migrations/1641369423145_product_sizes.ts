import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductSizes extends BaseSchema {
  protected tableName = 'product_sizes'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.string('size', 200)
      table.string('code_size', 200)
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
