import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ReviewCustomers extends BaseSchema {
  protected tableName = 'review_customers'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('customer_id')
      table.integer('product_id')
      table.string("title", 255)
      table.string("code_name", 100)
      table.string("title_sub", 255)
      table.double("rating")
      table.string("image", 255)
      table.text("description")
      table.boolean("status");
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
