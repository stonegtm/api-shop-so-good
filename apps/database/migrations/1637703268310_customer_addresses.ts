import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CustomerAddresses extends BaseSchema {
  protected tableName = "customer_addresses";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("customer_id", 11);
      table.string("address", 255);
      table.string("parish", 255);
      table.string("district", 255);
      table.string("province", 255);
      table.string("zipcode", 5);
      table.string("phone_number", 10);
      table.string("fullname", 255);
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
