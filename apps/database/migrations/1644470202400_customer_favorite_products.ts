import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CustomerFavoriteProducts extends BaseSchema {
  protected tableName = 'customer_favorite_products'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('customer_id')
      table.integer('product_id')
      table.boolean('status')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
