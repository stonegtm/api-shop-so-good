import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class PaymentSuccessullies extends BaseSchema {
  protected tableName = 'payment_successullies'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("order_number", 15)


      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
