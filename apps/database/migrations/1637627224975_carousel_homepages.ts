import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CarouselHomepages extends BaseSchema {
  protected tableName = "carousel_homepages";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("carousel_image", 255);
      table.string("carousel_alt", 255);
      table.integer("rating", 11);
      table.boolean("status");
      table.boolean("is_delete");
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
