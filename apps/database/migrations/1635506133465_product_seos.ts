import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class ProductSeos extends BaseSchema {
  protected tableName = "product_seos";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("product_id");
      table.string("meta_title");
      table.text("meta_description");
      table.string("meta_keywords",70);

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
