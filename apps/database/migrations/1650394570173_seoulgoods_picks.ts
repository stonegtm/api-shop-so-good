import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class SeoulgoodsPicks extends BaseSchema {
  protected tableName = 'seoulgoods_picks'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
