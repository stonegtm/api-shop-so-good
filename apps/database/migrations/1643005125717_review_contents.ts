import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ReviewContents extends BaseSchema {
  protected tableName = 'review_contents'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title', 255)
      table.string('title_code', 255)
      table.string('description_short', 255)
      table.text('description_full')
      table.string('image', 255)

      table.timestamps();

    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
