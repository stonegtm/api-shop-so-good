import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Promotions extends BaseSchema {
  protected tableName = 'promotions'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('image')
      table.string('banner_pc')
      table.string('banner_mobile')
      table.string('name', 255)
      table.string('code_name', 255)
      table.boolean("status");
      table.timestamps();
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
