import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CustomerProfiles extends BaseSchema {
  protected tableName = "customer_profiles";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("fullname", 150);
      table.integer("customer_id", 11);
      table.string("phone_number", 10);
      table.string("day", 2);
      table.string("month", 2);
      table.string("year", 4);
      table.integer("sex", 1);
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
