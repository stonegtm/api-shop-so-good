import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Districts extends BaseSchema {
  protected tableName = 'districts'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string("zip_code", 11);
      table.string("name_th", 150);
      table.string("name_en", 150);
      table.integer("amphure_id", 11);
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps();

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
