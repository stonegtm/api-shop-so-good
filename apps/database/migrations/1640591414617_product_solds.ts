import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductSolds extends BaseSchema {
  protected tableName = 'product_solds'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer("product_id", 11);
      table.string("name", 255);
      table.integer("quantity", 11);


      table.timestamps();
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
