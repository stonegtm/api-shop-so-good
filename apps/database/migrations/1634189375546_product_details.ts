import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class ProductDetails extends BaseSchema {
  protected tableName = 'product_details'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('product_id')
      table.text('description')
      table.text('content')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps()

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
