import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Amphures extends BaseSchema {
  protected tableName = 'amphures'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string("code", 4);
      table.string("name_th", 150);
      table.string("name_en", 150);
      table.integer("province_id", 5);
      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
       table.timestamps();

    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
