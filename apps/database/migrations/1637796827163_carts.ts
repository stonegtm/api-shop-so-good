import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Carts extends BaseSchema {
  protected tableName = "carts";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("customer_id");
      table.integer("product_id");
      table.integer("product_other_id");
      table.integer("quantity");
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
