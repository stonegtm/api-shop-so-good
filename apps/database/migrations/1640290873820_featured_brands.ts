import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class FeaturedBrands extends BaseSchema {
  protected tableName = "featured_brands";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.integer("rating", 1);
      table.boolean("status");
      table.boolean("is_delete");
      table.string("image_1", 255);
      table.string("image_2", 255);
      table.integer("index1", 11);
      table.integer("index2", 11);
      table.string("alt", 100);
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
