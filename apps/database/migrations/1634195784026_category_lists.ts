import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class CategoryLists extends BaseSchema {
  protected tableName = "category_lists";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("name");
      table.boolean("displayed");
      table.boolean("main");
      table.integer("id_category_main");
      table.string("img_cover");
      table.string("meta_title", 70);
      table.string("meta_description", 160);
      table.string("meta_keywords", 255);

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamps();
    });
  }

  public async down() {
    this.schema.dropTable(this.tableName);
  }
}
