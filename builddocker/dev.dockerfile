FROM node:16.13.1-alpine

RUN apk update && apk add bash

WORKDIR /src

ADD ./apps/package.json .

RUN npm i --global @adonisjs/cli

RUN npm install

EXPOSE 3333