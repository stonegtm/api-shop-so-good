FROM node:16.13.1-alpine

RUN apk update && apk add bash

WORKDIR /src

ADD ../apps/package.json .

RUN npm i --global @adonisjs/cli

RUN npm install

RUN npm install pm2 -global

# RUN npm run build

COPY --chown=node:node ./apps .

EXPOSE 3333